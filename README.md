# ALLergen

ALLergen is an application that allows you to easily identify allergens contained in products. 

Is this product safe for me? Can I try this new cake?

Answer these common allergy questions with ALLergen. Create your profile, add your allergies, and start scanning products.

Ingredients on product packages are automatically translated so you don't have to worry when traveling abroad.

**The application cannot guaranty the full accuracy of the data. Always verify a product's package before consumption**

## Support

If you are encountering issues with the application, please send an email at: charles.aubert@epitech.eu.