//
//  Products.swift
//  ALLergen
//
//  Created by Charles AUBERT on 1/13/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper

class ProductService: ObservableObject {
    public func getProductFromBarcode(barcode: String, preview: Bool, onSuccess: @escaping (ProductResponse) -> Void, onError: @escaping (Error) -> Void) {
        let URL = Endpoints.product + barcode + ".json?fields=" + ProductFields.combined.joined(separator: ",")
        
        Alamofire.request(URL).responseObject { (response: DataResponse<ProductResponse>) in
            switch response.result {
            case .success(let response):
                onSuccess(response)
            case .failure(let error as NSError):
                onError(error)
            }
        }
    }
    
    public func getTranslatedIngredients(ingredients: String, onSuccess: @escaping (TranslationResponse) -> Void, onError: @escaping (Error) -> Void) {
        let URL = Endpoints.translation
        let parameters: [String: Any] = [
            "key": "trnsl.1.1.20200425T092620Z.a09fd963ad436f8c.421100c7edba59c27a4fed78de084d641911eea4",
            "lang": "en",
            "text": ingredients
        ]

        Alamofire.request(URL, parameters: parameters).responseObject { (response: DataResponse<TranslationResponse>) in
            switch response.result {
            case .success(let response):
                onSuccess(response)
            case .failure(let error as NSError):
                onError(error)
            }
        }
    }
}
