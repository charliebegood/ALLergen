//
//  ProfileService.swift
//  ALLergen
//
//  Created by Charles AUBERT on 1/16/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import CoreData
import ObjectMapper
import SwiftUI
import Combine

class ProfileService: LocalService, ObservableObject {
    @Published var profiles: [ProfileRecord] = []
    var lastAdded: ProfileRecord? = nil
    
    override init(moc: NSManagedObjectContext) {
        super.init(moc: moc)
        _ = self.getProfiles()
    }
    
    func getProfiles() -> Bool {
        let request = NSFetchRequest<ProfileRecord>(entityName: "ProfileRecord")
        let fetched: [ProfileRecord]

        request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        do {
            try fetched = moc.fetch(request)
            self.profiles = fetched
        } catch {
            print("Failed fetching profiles.")
            return false
        }
        return true
    }
    
    func createTemporaryProfile() -> ProfileRecord {
        let profile = ProfileRecord(context: self.moc)

        profile.id = UUID()
        profile.allergies = []
        
        self.lastAdded = profile
        return profile
    }
    
    func saveTemporaryProfile(profile: ProfileRecord) -> Bool {
        self.profiles.append(profile)
        return saveChanges()
    }
    
    func createProfile() -> ProfileRecord {
        let profile = ProfileRecord(context: self.moc)

        profile.id = UUID()
        profile.allergies = []
        self.profiles.append(profile)
        return profile
    }
    
    func saveChanges() -> Bool {
        do {
            try self.moc.save()
            return true
        } catch {
            print(error)
            return false
        }
    }
    
    func deleteProfile(profile: ProfileRecord) {
        self.moc.delete(profile)
        if let index = self.profiles.firstIndex(of: profile) {
            self.profiles.remove(at: index)
        }
        _ = self.saveChanges()
    }
}
