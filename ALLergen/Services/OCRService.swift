//
//  OCRService.swift
//  ALLergen
//
//  Created by Charles AUBERT on 5/11/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import Combine
import UIKit
import SwiftyTesseract
import Vision

class OCRService: ObservableObject {
    let swiftyTesseract: SwiftyTesseract = SwiftyTesseract(language: .english, dataSource: Bundle.main, engineMode: .tesseractLstmCombined)
    var onGoingTask: DispatchWorkItem? = nil
    var onGoingExtractions: [AnyCancellable] = []

    @Published var showCamera: Bool = false
    @Published var capturedImage: UIImage? = nil
    @Published var loading: Bool = false

    public func analyzeImage(image: UIImage, completionHandler: @escaping (String?) -> Void){
        self.cleanTasks()
        self.loading = true
        var ingredients = ""
        let extractTasks = DispatchGroup()
        self.onGoingTask = DispatchWorkItem {
            self.getTextBoxes(image: image, completionHandler: { boxes in
                self.cropTextBoxes(image: image, boxes: boxes, completionHandler: { images in
                    for image in images {
                        self.onGoingExtractions.append(self.extractTextFromImage(image: image, group: extractTasks, completionHandler: { extracted in
                            ingredients += extracted?.replacingOccurrences(of: "^\\s*", with: "", options: .regularExpression, range: nil) ?? ""
                        }))
                    }
                    extractTasks.notify(queue: .main, execute: {
                        self.capturedImage = nil
                        if self.loading != false {
                            self.loading = false
                            completionHandler(ingredients)
                        }
                    })
                })
            })
        }
        if let task = self.onGoingTask {
            DispatchQueue.main.asyncAfter(deadline: .now(), execute: task)
        }
    }
    
    public func getTextBoxes(image: UIImage, completionHandler: @escaping ([VNTextObservation]) -> Void) {
        let handler = VNImageRequestHandler(
            cgImage: image.cgImage!,
            orientation: CGImagePropertyOrientation(image.imageOrientation),
            options: [VNImageOption: Any]()
        )
        let request = VNDetectTextRectanglesRequest(completionHandler: { request, error in
            if let observations = request.results as? [VNTextObservation] {
                completionHandler(observations)
            }
        })

        request.reportCharacterBoxes = true
        do {
            try handler.perform([request])
        } catch {
            print(error as Any)
            completionHandler([])
        }
    }
    
    public func cropTextBoxes(image: UIImage, boxes: [VNTextObservation], completionHandler: @escaping ([UIImage]) -> Void) {
        var images: [UIImage] = []
        let tasks = DispatchGroup()
        let queue = DispatchQueue(label: "Crop Image", qos: DispatchQoS.background)

        for box in boxes {
            tasks.enter()
            queue.async(execute: {
                self.cropTextBox(image: image, box: box, completionHandler: { (result: UIImage?) in
                    if let croppedImage = result {
                        images.append(croppedImage.scaledImage(1000) ?? croppedImage)
                    }
                    tasks.leave()
                })
            })
        }
        tasks.notify(queue: .main, execute: {
            completionHandler(images)
        })
    }
    
    public func cropTextBox(image: UIImage, box: VNTextObservation, completionHandler: @escaping (UIImage?) -> Void) {
        var transform = CGAffineTransform.identity
        
        transform = transform.scaledBy(x: image.size.width, y: -image.size.height)
        transform = transform.translatedBy(x: 0, y: -1)
        
        let rect = box.boundingBox.applying(transform)
        let scaleUp: CGFloat = 0.05
        let biggerRect = rect.insetBy(dx: -rect.size.width * scaleUp, dy: -rect.size.height * scaleUp)
        
        if let croppedImage = image.cgImage?.cropping(to: biggerRect) {
            completionHandler(UIImage(cgImage: croppedImage).preprocessedImage() ?? UIImage(cgImage: croppedImage))
        } else {
            completionHandler(nil)
        }
    }
    
    public func extractTextFromImage(image: UIImage, group: DispatchGroup, completionHandler: @escaping (String?) -> Void) -> AnyCancellable {
        group.enter()
        let extraction = swiftyTesseract.performOCRPublisher(on: image)
        .subscribe(on: DispatchQueue.global(qos: .background))
        .receive(on: DispatchQueue.main)
        .sink(
            receiveCompletion: { completion in
                group.leave()
            },
            receiveValue: { string in
                completionHandler(string)
            }
        )
        return extraction
    }

    private func cleanTasks() {
        self.onGoingTask?.cancel()
        self.onGoingTask = nil
        for extraction in self.onGoingExtractions {
            extraction.cancel()
            if let index = self.onGoingExtractions.firstIndex(of: extraction) {
                self.onGoingExtractions.remove(at: index)
            }
        }
    }
    
    public func cancelTasks() {
        self.loading = false
        self.cleanTasks()
    }
}
