//
//  AllergyService.swift
//  ALLergen
//
//  Created by Charles AUBERT on 1/16/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import Foundation
import SwiftUI
import CoreData

class AllergyService: LocalService, ObservableObject {
    func getAllergies() -> [AllergyRecord]? {
        let request = NSFetchRequest<AllergyRecord>(entityName: "AllergyRecord")
        let fetched: [AllergyRecord]

        request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        do {
            try fetched = moc.fetch(request)
            return fetched
        } catch {
            print("Failed fetching allergies.")
        }
        return nil
    }

    func createAllergy(name: String) -> AllergyRecord? {
        let allergy = AllergyRecord(context: self.moc)
        
        allergy.id = UUID()
        allergy.name = name
        return allergy
    }
    
    func fetchProfileAllergies(profile: ProfileRecord) -> [AllergyRecord]? {
        let request = NSFetchRequest<AllergyRecord>(entityName: "AllergyRecord")
        let fetched: [AllergyRecord]

        request.predicate = NSPredicate(format: "profile.id = %@", argumentArray: [profile.id ?? 0])
        request.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        do {
            try fetched = moc.fetch(request)
            return fetched
        } catch {
            print("Failed fetching profile allergies.")
        }
        return nil
    }
    
    func removeAllergy(allergy: AllergyRecord) {
        self.moc.delete(allergy)
    }

    func saveChanges() -> Bool {
        do {
            try self.moc.save()
            return true
        } catch {
            print(error)
            return false
        }
    }
}
