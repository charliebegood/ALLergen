//
//  ObjectService.swift
//  ALLergen
//
//  Created by Charles AUBERT on 1/16/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import Foundation
import CoreData

class LocalService {
    var moc: NSManagedObjectContext
    
    init(moc: NSManagedObjectContext) {
        self.moc = moc
    }
}
