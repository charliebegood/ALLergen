//
//  AnalyzeService.swift
//  ALLergen
//
//  Created by Charles AUBERT on 4/25/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import Combine
import UIKit
import SwiftyTesseract
import Vision

class AnalyzeService: ObservableObject {
    var profileService: ProfileService
    var allergyService: AllergyService
    let swiftyTesseract: SwiftyTesseract = SwiftyTesseract(language: .english, dataSource: Bundle.main, engineMode: .tesseractLstmCombined)
    
    init(profileService: ProfileService, allergyService: AllergyService) {
        self.profileService = profileService
        self.allergyService = allergyService
    }
    
    public func analyzeIngredients(ingredients: String) -> [String: [ProfileRecord]] {
        var result: [String: [ProfileRecord]] = ["safe": [], "unsafe": []]
        
        let ingredients = ingredients.lowercased()
        for profile in profileService.profiles {
            let allergies = allergyService.fetchProfileAllergies(profile: profile)
            var safe = true
            
            for allergy in allergies ?? [] {
                if ingredients.contains(allergy.name?.lowercased() ?? "") {
                    safe = false
                    break
                }
            }
            result[safe ? "safe" : "unsafe"]?.append(profile)
        }
        return result
    }
}
