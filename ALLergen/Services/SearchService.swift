//
//  Search.swift
//  ALLergen
//
//  Created by Charles AUBERT on 1/14/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import CoreData
import SwiftUI

class SearchService: LocalService, ObservableObject {
    var onGoingSearch: DispatchWorkItem? = nil
    
    func getRecentQueries(length: Int) -> [SearchSuggestion] {
        let request = SearchSuggestion.createFetchRequest()
        let sort = NSSortDescriptor(key: "date", ascending: false)
        var result = [SearchSuggestion]()

        request.sortDescriptors = [sort]
        do {
            result = try self.moc.fetch(request)
        } catch {
            print("Recent search fetch failed")
        }
        return Array(result.prefix(length))
    }
    
    func saveNewQuery(title: String) {
        let request = SearchSuggestion.createFetchRequest()
        let sort = NSSortDescriptor(key: "date", ascending: false)
        var result = [SearchSuggestion]()
        
        request.sortDescriptors = [sort]
        request.fetchLimit = 1
        do {
            result = try self.moc.fetch(request)
        } catch {
            print("Recent search fetch failed")
        }
        if (result.count > 0 && result[0].title ?? "" == title) {
            return
        }
        let query = NSEntityDescription.insertNewObject(forEntityName: "SearchSuggestion", into: self.moc ) as! SearchSuggestion
        query.title = title
        query.date = NSDate()
        do {
            try self.moc.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
            return
        }
    }

    public func getSearchResults(query: String, onSuccess: @escaping (SearchResponse) -> Void, onError: @escaping (Error) -> Void) {
        let escapedQuery = "&search_terms=" + (query.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "")
        let URL = Endpoints.search + "?" + SearchOptions.combined.joined(separator: "&") + "&page=1" + escapedQuery
         
        if (query == "") {
            return
        }
        self.terminateOnGoingSearch()
        self.onGoingSearch = DispatchWorkItem {
            Alamofire.request(URL).responseObject { (response: DataResponse<SearchResponse>) in
                switch response.result {
                    case .success(let response):
                        onSuccess(response)
                    case .failure(let error as NSError):
                        onError(error)
                }
            }
        }
        DispatchQueue.main.async(execute: self.onGoingSearch ?? DispatchWorkItem { return })
    }
    
    public func getNewPage(query: String, page: Int, onSuccess: @escaping (SearchResponse) -> Void, onError: @escaping (Error) -> Void) {
        let escapedQuery = "&search_terms=" + (query.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "")
        let URL = Endpoints.search + "?" + SearchOptions.combined.joined(separator: "&") + "&page=" + String(page) + escapedQuery
         
        if (query == "") {
            return
        }
        let workItem = DispatchWorkItem {
            Alamofire.request(URL).responseObject { (response: DataResponse<SearchResponse>) in
                switch response.result {
                    case .success(let response):
                        onSuccess(response)
                    case .failure(let error as NSError):
                        onError(error)
                }
            }
        }
        DispatchQueue.main.async(execute: workItem)
    }
    
    private func terminateOnGoingSearch() {
        if (self.onGoingSearch != nil) {
            self.onGoingSearch?.cancel()
        }
    }
}
