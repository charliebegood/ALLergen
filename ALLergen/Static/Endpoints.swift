//
//  Endpoints.swift
//  ALLergen
//
//  Created by Charles AUBERT on 1/13/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import Foundation

struct Endpoints {
    static let common = "https://world.openfoodfacts.org"
    static let product = Endpoints.common + "/api/v0/product/"
    static let search = Endpoints.common + "/cgi/search.pl"
    static let translation = "https://translate.yandex.net/api/v1.5/tr.json/translate"
}
