//
//  Fields.swift
//  ALLergen
//
//  Created by Charles AUBERT on 1/13/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import Foundation

struct ProductFields {
    static let name = "product_name"
    static let code = "code"
    static let brands = "brands"
    static let ingredients = "ingredients_text"
    static let frontImage = "image_front_url"
    static let combined = [ProductFields.name, ProductFields.code, ProductFields.brands, ProductFields.ingredients, ProductFields.frontImage]
}

struct SearchProductFields {
    static let name = "product_name"
    static let code = "code"
    static let brands = "brands"
    static let frontImage = "image_front_url"
    static let combined = [SearchProductFields.name, ProductFields.code, SearchProductFields.brands, SearchProductFields.frontImage]
}

struct ProductResponseFields {
    static let product = "product"
    static let status = "status"
    static let statusMessage = "status_verbose"
    static let code = "code"
}

struct SearchResponseFields {
    static let page = "page"
    static let products = "products"
    static let pageSize = "page_size"
    static let count = "count"
    static let skip = "40"
}

struct TranslationResponseFields {
    static let code = "code"
    static let text = "text"
    static let lang = "lang"
}

struct SearchOptions {
    static let action = "action=process"
    static let sortBy = "sort_by=unique_scans_n"
    static let pageSize = "page_size=20"
    static let json = "json=1"
    static let fields = "fields=" + SearchProductFields.combined.joined(separator: ",")
    static let combined = [SearchOptions.action, SearchOptions.sortBy, SearchOptions.pageSize, SearchOptions.json, SearchOptions.fields]
}
