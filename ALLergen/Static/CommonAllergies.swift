//
//  CommonAllergies.swift
//  ALLergen
//
//  Created by Charles AUBERT on 3/11/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import Foundation

let commonAllergyNames: [String] = ["eggs", "celery", "corn", "shellfish", "fish", "wheat", "sulfites", "soy", "peanuts", "sesame", "lupin", "nuts", "mushroom", "milk", "mustard", "molluscs"]

let commonAllergies: [Allergy] = [Allergy(name: "eggs"), Allergy(name: "celery"), Allergy(name: "corn"), Allergy(name: "shellfish"), Allergy(name: "fish"), Allergy(name: "wheat"), Allergy(name: "sulfites"), Allergy(name: "soy"), Allergy(name: "peanuts"), Allergy(name: "sesame"), Allergy(name: "lupin"), Allergy(name: "nuts"), Allergy(name: "mushroom"), Allergy(name: "milk"), Allergy(name: "mustard"), Allergy(name: "molluscs")]
