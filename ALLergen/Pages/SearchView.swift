//
//  SearchView.swift
//  ALLergen
//
//  Created by Charles AUBERT on 12/4/19.
//  Copyright © 2019 Charles AUBERT. All rights reserved.
//

import SwiftUI

struct SearchView: View {
    @Environment(\.colorScheme) var colorScheme: ColorScheme
    
    @State var searchBarStateManager: SearchBarStateManager = SearchBarStateManager()
    
    var body: some View {
        NavigationView {
            VStack {
                SearchBar(searchBarStateManager: self.$searchBarStateManager)
                    .padding([.leading, .trailing], 10)
                
                SearchBody(searchBarStateManager: self.$searchBarStateManager)
            }
            .padding(.top, 10)
            .navigationBarTitle(Text("Search").foregroundColor(.white), displayMode: .large)
        }
    }
}

struct SearchView_Previews: PreviewProvider {
    static var previews: some View {
        SearchView()
    }
}
