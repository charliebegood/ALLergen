//
//  ContentView.swift
//  ALLergen
//
//  Created by Charles AUBERT on 12/4/19.
//  Copyright © 2019 Charles AUBERT. All rights reserved.
//

import SwiftUI
import BarcodeScanner

struct ScannerView: View {
    @EnvironmentObject var productService: ProductService
    @EnvironmentObject var analyzeService: AnalyzeService
    
    @State var loading: Bool = false
    @State var visible: Bool = true
    @State var torchMode: TorchMode = .off
    @State var productCardPosition = CardPosition.bottom
    @State var maxProductCardPosition = CardPosition.top
    @State var product: Product = Product(JSON: [ProductFields.name: "Name", ProductFields.brands: "Brand", ProductFields.ingredients: "Ingredients"])!

    var body: some View {
        GeometryReader() { geometry in
            ZStack() {
                BarcodeScannerView(torchMode: self.$torchMode, product: self.$product, productCardPosition: self.$productCardPosition, maxProductCardPosition: self.$maxProductCardPosition, loading: self.$loading)
                    .edgesIgnoringSafeArea(.all)

                VStack() {
                    HStack(alignment: .center) {
                        FloatingButton(title: "flashlight.off.fill", selectedTitle: "flashlight.on.fill", selectable: true, selected: self.torchMode == .off ? .constant(false) : .constant(true), action: {
                            self.torchMode = self.torchMode == .off ? .on : .off
                        })
                        
                        Spacer()
                        
//                        FloatingButton(title: "bubble.right.fill", selected: .constant(false), action: {
//                            self.ocrService.analyzeImage(image: UIImage(named: "ocr2")!)
//                            self.product.reset()
//                            self.productCardPosition = .middle
//                            self.maxProductCardPosition = .middle
//                            self.loading = true
//                            "3168930009030"
//                            "7622210721389"
//                            self.productService.getProductFromBarcode(barcode: "3168930009030", preview: true, onSuccess: {(productResponse: ProductResponse) in
//                                self.loading = false
//                                if (productResponse.statusMessage ?? "" != "product found") {
//                                    self.product.valid = false
//                                } else if let product = productResponse.product {
//                                    self.product = product
//                                    self.maxProductCardPosition = self.product.validate() ? .top : .middle
//                                }
//                            }, onError: {(error: Error) in
//                                print(error)
//                                self.loading = false
//                                self.maxProductCardPosition = self.product.validate() ? .top : .middle
//                            })
//                        })
                    }
                    .padding([.leading, .trailing], 16)
                    
                    Spacer()
                }
                
                ProductCard(product: self.$product, productCardPosition: self.$productCardPosition, maxProductCardPosition: self.$maxProductCardPosition, loading: self.$loading)
            }
        }
    }
}

//#if DEBUG
//struct ScannerView_Previews: PreviewProvider {
//    static var previews: some View {
//        ScannerView(navigationStateManager: .constant(NavigationStateManager()), position: 1)
//            .previewDevice(PreviewDevice(rawValue: "iPhone 11"))
//            .previewDisplayName("iPhone 11")
//    }
//}
//#endif
