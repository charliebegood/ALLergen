//
//  OnboardingCard.swift
//  ALLergen
//
//  Created by Charles AUBERT on 5/9/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import SwiftUI

struct OnboardingCard: View {
    var image: String
    var text: String

    var body: some View {
        VStack {
            Image(image)
                .resizable()
                .scaledToFit()
                .background(Color("Spring Green"))
                .padding([.leading, .trailing], 30)
                .padding([.bottom], 25)
            
            Text(text)
                .font(.system(size: 20, weight: .bold, design: .default))
                .foregroundColor(.white)
                .frame(maxWidth: 250)
                .multilineTextAlignment(.center)
        }
        
    }
}

struct OnboardingCard_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingCard(image: "onboardin_scan", text:" ")
    }
}
