//
//  OnboardingView.swift
//  ALLergen
//
//  Created by Charles AUBERT on 5/9/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import SwiftUI

struct OnboardingView: View {
    @State var currentPageIndex = 0
    @Binding var showing: Bool
    @Binding var tab: Int

    var cards = [
        UIHostingController(rootView: OnboardingCard(image: "onboarding_scan", text: "Scan products to analyze their ingredients")),
        UIHostingController(rootView: OnboardingCard(image: "onboarding_search", text: "Search and discover new products")),
        UIHostingController(rootView: OnboardingCard(image: "onboarding_profile", text: "Create your profile to get a personalized analysis"))
    ]

    var body: some View {
        VStack {
            Spacer()
            PageViewController(currentPageIndex: $currentPageIndex, viewControllers: cards)
                .frame(maxHeight: 550)
            
            PageControl(currentPageIndex: $currentPageIndex, numberOfPages: cards.count)
            
            Spacer()
                .frame(height: 20)
            Button(action: {
                self.currentPageIndex += self.currentPageIndex + 1 == 3 ? 0 : 1
            }) {
                if (self.currentPageIndex < 2) {
                    Image(systemName: "arrow.right.circle.fill")
                        .resizable()
                        .foregroundColor(.white)
                        .frame(width: 45, height: 45)
                } else {
                    StartButton(action: {
                        self.showing = false
                        self.tab = 3
                    }, title: "Get started", invertedColor: true)
                }
            }
            Spacer()
        }
        .background(Color("Spring Green"))
        .edgesIgnoringSafeArea(.all)
        
    }
}

struct OnboardingView_Previews: PreviewProvider {
    static var previews: some View {
        OnboardingView(showing: .constant(true), tab: .constant(0))
    }
}
