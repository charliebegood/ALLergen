//
//  OCRView.swift
//  ALLergen
//
//  Created by Charles AUBERT on 5/10/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import SwiftUI
import Combine

struct OCRView: View {
    @Binding var image: UIImage?
    @Binding var showCaptureImageView: Bool

    @State var showLanguageInfo: Bool = !UserDefaults.standard.bool(forKey: "languageNotice")

    var body: some View {
        ZStack {
            if (showCaptureImageView) {
                CaptureImageView(isShown: $showCaptureImageView, image: $image)
            }
        }
        .alert(isPresented: self.$showLanguageInfo) {
            Alert(title: Text("Notice"), message: Text("English is currently the only supported language."), dismissButton: .default(Text("I understand"), action: {
                UserDefaults.standard.set(true, forKey: "languageNotice")
            }))
        }
        .edgesIgnoringSafeArea(.all)
    }
}
