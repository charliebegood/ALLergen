//
//  NavigationManager.swift
//  ALLergen
//
//  Created by Charles AUBERT on 12/6/19.
//  Copyright © 2019 Charles AUBERT. All rights reserved.
//

import SwiftUI
import Combine

struct NavigationManager: View {
    @State var selection = 2
    @State var onboarding: Bool = false
    @EnvironmentObject var profileService: ProfileService

    var body: some View {
        VStack {
            if (!onboarding) {
                TabView(selection: self.$selection) {
                    SearchView()
                        .tabItem {
                            Image(systemName: "magnifyingglass")
                            Text("Search")
                        }
                        .tag(1)

                    ScannerView()
                        .tabItem {
                            Image(systemName: "barcode.viewfinder")
                            Text("Scanner")
                        }
                        .tag(2)
                    
                    ProfilesView()
                        .tabItem {
                            Image(systemName: "person.2.fill")
                            Text("Profiles")
                        }
                        .tag(3)
                }
                .accentColor(Color(#colorLiteral(red: 0.06300000101, green: 0.5099999905, blue: 0.3100000024, alpha: 1)))
                
            } else {
                OnboardingView(showing: self.$onboarding, tab: self.$selection)
            }
        }
        .onAppear(perform: {
            if self.profileService.profiles.count == 0 {
                self.onboarding = true
            }
        })
    }
}

struct NavigationManager_Previews: PreviewProvider {
    static var previews: some View {
        NavigationManager()
    }
}

extension UINavigationController {
    override open func viewDidLoad() {
        super.viewDidLoad()
      
        let standardAppearance = UINavigationBarAppearance()
        standardAppearance.backgroundColor = UIColor(named: "Dark Green")
        standardAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        standardAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
        standardAppearance.shadowColor = .clear
        
        let compactAppearance = UINavigationBarAppearance()
        compactAppearance.backgroundColor = UIColor(named: "Dark Green")
        compactAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        compactAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
        compactAppearance.shadowColor = .clear
        
        let scrollEdgeAppearance = UINavigationBarAppearance()
        scrollEdgeAppearance.backgroundColor = UIColor(named: "Dark Green")
        scrollEdgeAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
        scrollEdgeAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
        scrollEdgeAppearance.shadowColor = .clear
        
        navigationBar.standardAppearance = standardAppearance
        navigationBar.compactAppearance = compactAppearance
        navigationBar.scrollEdgeAppearance = scrollEdgeAppearance
        
        self.navigationBar.tintColor = .white

    }
}
