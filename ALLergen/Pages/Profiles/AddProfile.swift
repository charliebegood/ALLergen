//
//  AddProfile.swift
//  ALLergen
//
//  Created by Charles AUBERT on 3/11/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import SwiftUI
import CoreData

struct AddProfile: View {
    @EnvironmentObject var profileService: ProfileService
    @EnvironmentObject var allergyService: AllergyService
    @Environment(\.colorScheme) var colorScheme: ColorScheme

    @State var profile: ProfileRecord
    @State var allergies: [AllergyRecord] = []
    @State private var name: String = ""
    @State private var showingImagePicker = false
    @State private var inputImage: UIImage? = nil

    init(profile: ProfileRecord) {
        self._profile = State(initialValue: profile)
    }

    var body: some View {
        VStack {
            
            // Header
            VStack(alignment: .center) {
                
                // Top Buttons
                ModalTopBar(title: "New Profile", enabled: .constant(self.allergies.count > 0 && self.name != "" && self.name != "Name"), cancelAction: {
                    UIApplication.shared.windows[0].rootViewController?.dismiss(animated: true, completion: { })
                }, doneAction: {
                    self.profile.name = self.name
                    if (self.inputImage != nil) {
                        self.profile.setValue(self.inputImage!.pngData(), forKey: "image")
                    }
                    self.profileService.lastAdded = nil
                    _ = self.profileService.saveTemporaryProfile(profile: self.profile)
                    UIApplication.shared.windows[0].rootViewController?.dismiss(animated: true, completion: { })
                })
                
                //Profile Image
                VStack(alignment: .center) {
                    Button(action: {
                        self.showingImagePicker.toggle()
                    }) {
                        ProfilePicture(name: self.$name, picture: self.$inputImage, editing: .constant(true), size: .big)
                    }
                    .buttonStyle(PlainButtonStyle())
                    
                    TextField("Name", text: $name)
                        .accentColor(Color("Light Green"))
                        .multilineTextAlignment(.center)
                        .font(.system(size: 22, weight: .medium, design: .default))
                }
                .sheet(isPresented: $showingImagePicker) {
                    ImagePicker(image: self.$inputImage)
                }
            }
            .padding([.top, .bottom], 10)
            .padding([.leading, .trailing], 14)
            .background(Color("Header Background"))

            AllergiesList(profile: self.$profile, allergies: self.$allergies)
            
            Spacer()
        }
        .background(self.colorScheme == .dark ? Color.black : Color.white)
        .edgesIgnoringSafeArea(.bottom)
    }
}
