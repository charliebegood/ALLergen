//
//  ProfilesView.swift
//  ALLergen
//
//  Created by Charles AUBERT on 12/4/19.
//  Copyright © 2019 Charles AUBERT. All rights reserved.
//

import SwiftUI
import CoreData

struct ProfilesView: View {
    @Environment(\.colorScheme) var colorScheme: ColorScheme
    @EnvironmentObject var profileService: ProfileService
    @EnvironmentObject var allergyService: AllergyService

    @State var visible: Bool = false
    @State var tint: UIColor = UIColor.white
    @State var addingProfile: Bool = false
    
    var body: some View {
        NavigationView {
            VStack() {
                if (self.profileService.profiles.count > 0) {
                    List {
                        ForEach(self.profileService.profiles, id: \.id) { (profile: ProfileRecord) in
                            NavigationLink(destination: ProfileView(profile: Binding.constant(profile))) {
                                ProfileRow(profile: Binding(get: { profile }, set: { _ = $0 }))
                            }
                        }
                    }
                } else {
                    EmptyProfilesView(addingProfile: self.$addingProfile)
                }
                Spacer()
            }
            .navigationBarTitle(Text("Profiles").foregroundColor(.white), displayMode: .large)
            .navigationBarItems(
                trailing:
                    Button(action: {
                        self.addingProfile.toggle()
                    }) {
                        Image(systemName: "person.badge.plus.fill")
                            .font(.system(size: 25, weight: .bold, design: .default))
                            .foregroundColor(self.colorScheme == .dark ? Color("Spring Green") : .white)
                    }
            )
        }
        .onAppear(perform: {
            if self.profileService.profiles.count == 0 {
                self.addingProfile = true
            }
        })
        .sheet(isPresented: self.$addingProfile, onDismiss: {
            self.addingProfile = false
            if let profile = self.profileService.lastAdded {
                self.profileService.deleteProfile(profile: profile)
            }
        }, content: {
            AddProfile(profile: self.profileService.createTemporaryProfile())
                .environmentObject(self.profileService)
                .environmentObject(self.allergyService)
                .environment(\.editMode, Binding.constant(EditMode.active))
        })
    }
}

struct ProfilesView_Previews: PreviewProvider {
    static var previews: some View {
        ProfilesView()
    }
}

struct EmptyProfilesView: View {
    @Binding var addingProfile: Bool
    
    var body: some View {
        VStack {
            Spacer()
            VStack(alignment: .center, spacing: 15) {
                Text("To know which products are safe and get recommandations, create your profile")
                    .font(.system(size: 16, weight: .semibold, design: Font.Design.rounded))
                    .foregroundColor(Color.systemGray)
                    .padding([.trailing, .leading], 90)
                    .multilineTextAlignment(.center)
                StartButton(action: {
                    self.addingProfile = true
                }, title: "Get started")
            }
            Spacer()
        }
    }
}

struct EmptyProfilesView_Previews: PreviewProvider {
    static var previews: some View {
        EmptyProfilesView(addingProfile: .constant(false))
    }
}
