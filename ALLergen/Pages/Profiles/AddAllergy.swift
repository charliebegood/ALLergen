//
//  AddAllergy.swift
//  ALLergen
//
//  Created by Charles AUBERT on 3/11/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import SwiftUI
import Combine
import ASCollectionView_SwiftUI

struct AddAllergy: View {
    @EnvironmentObject var allergyService: AllergyService
    @Environment(\.colorScheme) var colorScheme: ColorScheme

    @State var customName: String = ""
    @State var selected: Bool = false
    @State var selectedAllergies: [Allergy] = []
    
    @Binding var profile: ProfileRecord
    @Binding var showing: Bool
    
    var currentAllergies: [AllergyRecord]
    var availableAllergies: [Allergy] = commonAllergies
    
    init(profile: Binding<ProfileRecord>, showing: Binding<Bool>, currentAllergies: [AllergyRecord]) {
        _profile = profile
        _showing = showing
        self.currentAllergies = currentAllergies
        for currentAllergy in currentAllergies {
            availableAllergies.removeAll(where: { $0.name == (currentAllergy.name ?? "")})
        }
    }
    
    var body: some View {
        VStack {

            //Topbar
            ModalTopBar(title: "Add Allergy", enabled: .constant(self.selectedAllergies.count > 0 || self.customName != ""), cancelAction: {
                self.showing = false
            }, doneAction: {
                for item in self.selectedAllergies {
                    if let allergy = self.allergyService.createAllergy(name: item.name) {
                        self.profile.addToAllergies(allergy)
                    }
                }
                if self.customName != "" {
                    if let allergy = self.allergyService.createAllergy(name: self.customName) {
                        self.profile.addToAllergies(allergy)
                    }
                }
                self.showing = false
            })
            .padding([.top, .bottom], 10)
            .padding([.leading, .trailing], 14)
            .background(Color("Header Background"))

            //Common collection
            VStack(alignment: .leading) {
                Text("Common")
                    .font(.system(size: 22, weight: .bold, design: .default))

                ASCollectionView(data: self.availableAllergies) { item, _ in
                    Button(action: {
                        if let index = self.selectedAllergies.firstIndex(of: item) {
                            self.selectedAllergies.remove(at: index)
                        } else {
                            self.selectedAllergies.append(item)
                        }
                    }) {
                        VStack(alignment: .center, spacing: 5) {
                            Image(item.name)
                                .resizable()
                                .frame(width: 40, height: 40)
                                .accentColor(self.selectedAllergies.contains(item) ? .white : Color("Light Green"))
                                .background(Circle().foregroundColor(self.selectedAllergies.contains(item) ? Color("Spring Green") : self.colorScheme == .dark ? .black : .white))

                            Text((item.name).capitalizingFirstLetter())
                                .font(.system(size: 13, weight: .regular, design: .default))
                                .foregroundColor(self.selectedAllergies.contains(item) ? self.colorScheme == .dark ? .white : .black : .systemGray)
                        }
                    }
                }
                .layout {
                    let layout = UICollectionViewFlowLayout()
                    let itemWidth = ((UIScreen.main.bounds.width - 60) / CGFloat(4)).rounded(.down)
                    
                    layout.minimumLineSpacing = 15
                    layout.itemSize = CGSize(width: itemWidth, height: 60)
                    return layout
                }
                .shrinkToContentSize(dimension: .vertical)
                
                Text("Other")
                    .font(.system(size: 22, weight: .bold, design: .default))
                    .padding([.top], 10)
                
                TextField("Custom Allergy", text: $customName)
                    .accentColor(Color("Light Green"))
                    .padding([.top, .bottom], 10)
                    .padding([.leading, .trailing], 14)
                    .background(Color.systemGray6)
                    .cornerRadius(5.0)
            }
            .padding([.leading, .trailing], 14)
            .padding([.top], 10)

            Spacer()
        }
        .keyboardAdaptive()
        .background(self.colorScheme == .dark ? Color.black : Color.white)
    }
}

struct AddAllergy_Previews: PreviewProvider {
    static var previews: some View {
        AddAllergy(profile: .constant(.init()), showing: .constant(true), currentAllergies: [])
    }
}
