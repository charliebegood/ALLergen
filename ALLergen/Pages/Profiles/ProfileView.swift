//
//  Profile.swift
//  ALLergen
//
//  Created by Charles AUBERT on 4/13/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import SwiftUI

struct ProfileView: View {
    @EnvironmentObject var profileService: ProfileService
    @EnvironmentObject var allergyService: AllergyService
    @Environment(\.presentationMode) var presentationMode
    @Environment(\.colorScheme) var colorScheme: ColorScheme
    @Environment(\.editMode) var editMode

    @Binding var profile: ProfileRecord
    
    @State var allergies: [AllergyRecord] = []
    @State var profileImage: UIImage? = nil
    @State private var showingImagePicker = false
    
    var body: some View {
        VStack {
            HStack() {
                Spacer()
                
                VStack(alignment: .center) {
                    Button(action: {
                        self.showingImagePicker.toggle()
                    }) {
                        ProfilePicture(name: self.$profile.name ?? "", picture: self.$profileImage, editing: Binding(get: { self.editMode?.wrappedValue == .active }, set: { _ = $0 }), size: .big)
                    }
                    .disabled(self.editMode?.wrappedValue == .inactive)
                    .sheet(isPresented: $showingImagePicker, onDismiss: {
                        if (self.profileImage != nil) {
                            self.profile.setValue(self.profileImage!.pngData(), forKey: "image")
                            _ = self.profileService.saveChanges()
                        }
                    }) {
                        ImagePicker(image: self.$profileImage)
                    }
                    
                    TextField("Name", text: (self.$profile.name ?? ""), onCommit: {
                        _ = self.profileService.saveChanges()
                    })
                        .multilineTextAlignment(.center)
                        .font(.system(size: 28, weight: .bold, design: .default))
                        .foregroundColor(.white)
                        .disabled(self.editMode?.wrappedValue != .active ? true : false)
                }
                .padding([.bottom], 15)
                
                Spacer()
            }
            .background(Color("Dark Green"))
            
            AllergiesList(profile: self.$profile, allergies: self.$allergies)
            
            if (self.editMode?.wrappedValue == .active) {
                Button(action: {
                    self.presentationMode.wrappedValue.dismiss()
                    self.profileService.deleteProfile(profile: self.profile)
                }) {
                    Text("Delete")
                        .textStyle(CancelButtonStyle())
                }
                .padding(.bottom, 20)
            }
        }
        .navigationBarItems(trailing: EditButton().foregroundColor(self.colorScheme == .dark ? Color("Spring Green") : .white))
        .navigationBarTitle("", displayMode: .inline)
        .onAppear(perform: {
            self.allergies = self.allergyService.fetchProfileAllergies(profile: self.profile) ?? []
            self.profileImage = UIImage(data: self.profile.image ?? Data())
        })
    }
}

struct ProfileView_Previews: PreviewProvider {
    static var previews: some View {
        ProfileView(profile: .constant(ProfileRecord()))
    }
}
