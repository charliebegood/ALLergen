//
//  ModalTopBar.swift
//  ALLergen
//
//  Created by Charles AUBERT on 4/22/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import SwiftUI

struct ModalTopBar: View {
    @State var title: String
    @Binding var enabled: Bool

    var cancelAction: () -> Void
    var doneAction: () -> Void

    var body: some View {
        VStack {
            HStack {
                Button(action: cancelAction) {
                    Text("Cancel")
                        .textStyle(CancelButtonStyle())
                }

                Spacer()

                Text(self.title)
                    .font(.system(size: 20, weight: .medium, design: .default))

                Spacer()

                Button(action: doneAction) {
                    if (self.enabled) {
                        Text("Done")
                            .textStyle(DoneButtonStyle())
                    } else {
                        Text("Done")
                            .textStyle(DisabledButtonStyle())
                    }
                }
                .disabled(!self.enabled)
            }
        }
    }
}
