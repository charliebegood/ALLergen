//
//  HostingViewController.swift
//  ALLergen
//
//  Created by Charles AUBERT on 1/12/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import Foundation
import UIKit
import SwiftUI

class HostingController<Content> : UIHostingController<Content> where Content : View {
    @objc override dynamic open var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
}
