//
//  AllergyPicture.swift
//  ALLergen
//
//  Created by Charles AUBERT on 4/23/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import SwiftUI

struct AllergyPicture: View {
    @State var name: String

    var fullSize: Bool
    var size: CGFloat = 40

    init(name: State<String>, fullSize: Bool = true) {
        _name = name
        self.fullSize = fullSize
        self.size = fullSize ? 40 : 20
    }

    var body: some View {
        ZStack() {
            if (self.name != "" && UIImage(named: self.name) != nil) {
                Image(self.name)
                    .resizable()
                    .frame(width: self.size, height: self.size)
                    .foregroundColor(Color("Light Green"))
             } else {
                Text(self.name.prefix(1))
                    .frame(width: self.size, height: self.size)
                    .font(.system(size: self.fullSize ? 20 : 10, weight: .bold, design: .rounded))
                    .foregroundColor(Color("Light Green"))
                    .overlay(
                        Circle()
                            .stroke(Color("Light Green"), lineWidth: self.fullSize ? 1 : 0.5)
                    )
             }
        }
    }
}
