//
//  SafetyList.swift
//  ALLergen
//
//  Created by Charles AUBERT on 4/25/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import SwiftUI

struct SafetyList: View {
    @Binding var profiles: [String: [ProfileRecord]]

    var body: some View {
        VStack(spacing: 30) {
            if (profiles["safe"]?.count ?? 0 > 0) {
                VStack(alignment: .leading) {
                    Text("Safe")
                        .font(.system(size: 20, weight: .bold, design: .default))

                    ForEach(profiles["safe"] ?? [], id: \.id) {(profile: ProfileRecord) in
                        SafetyListProfileRow(profile: .constant(profile), safe: true)
                    }
                }
            }

            if (profiles["unsafe"]?.count ?? 0 > 0) {
                VStack(alignment: .leading) {
                    Text("Not Safe")
                        .font(.system(size: 20, weight: .bold, design: .default))

                    ForEach(profiles["unsafe"] ?? [], id: \.id) {(profile: ProfileRecord) in
                        SafetyListProfileRow(profile: .constant(profile), safe: false)
                    }
                }
            }
        }
    }
}

struct SafetyList_Previews: PreviewProvider {
    static var previews: some View {
        SafetyList(profiles: .constant(["safe": [], "unsafe": []]))
    }
}

struct SafetyListProfileRow: View {
    @Binding var profile: ProfileRecord

    var safe: Bool

    var body: some View {
        HStack {
            ProfilePicture(name: .constant(profile.name ?? ""), picture: .constant(UIImage(data: profile.image ?? Data())), editing: .constant(false), size: .small)

            Text(profile.name ?? "")

            Spacer()

            Image(systemName: safe ? "checkmark.circle.fill" : "xmark.circle.fill")
                .foregroundColor(Color(safe ? "Light Green" : "Flame"))
        }
    }
}
