//
//  AllergyRow.swift
//  ALLergen
//
//  Created by Charles AUBERT on 3/11/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import SwiftUI

struct AllergyRow: View {
    @Binding var allergy: AllergyRecord

    var body: some View {
        HStack(alignment: .center, spacing: 16) {
            AllergyPicture(name: State(initialValue: self.allergy.name ?? ""))

            Text(self.allergy.name ?? "")
                .font(.system(size: 17, weight: .medium, design: .default))

            Spacer()
        }
        .padding([.top, .bottom], 5)
        .padding([.trailing], 14)
    }
}
