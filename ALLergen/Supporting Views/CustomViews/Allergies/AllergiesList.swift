//
//  AllergiesList.swift
//  ALLergen
//
//  Created by Charles AUBERT on 4/17/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import SwiftUI

struct AllergiesList: View {
    @EnvironmentObject var profileService: ProfileService
    @EnvironmentObject var allergyService: AllergyService
    @Environment(\.colorScheme) var colorScheme: ColorScheme

    @Binding var profile: ProfileRecord
    @Binding var allergies: [AllergyRecord]

    @State var addingAllergy: Bool = false

    init(profile: Binding<ProfileRecord>, allergies: Binding<[AllergyRecord]>) {
        _profile = profile
        _allergies = allergies
        UITableView.appearance().backgroundColor = .clear
    }

    var body: some View {
        VStack() {
            //Allergy controls
            HStack(alignment: .top) {
                Text("Allergies")
                    .font(.system(size: 25, weight: .bold, design: .default))

                Spacer()

                Button(action: {
                    self.addingAllergy = true
                }) {
                    HStack {
                        Text("Add")
                            .font(.system(size: 20, weight: .regular, design: .default))
                            .foregroundColor(Color("Spring Green"))

                        Image(systemName: "plus.circle.fill")
                            .resizable()
                            .frame(width: 20, height: 20)
                            .accentColor(Color("Spring Green"))
                    }
                }
            }
            .padding([.leading, .trailing], 14)

            //Allergy List
            if (self.allergies.count > 0) {
                List {
                    ForEach(self.allergies) { allergy in
                        AllergyRow(allergy: Binding(get: { allergy }, set: { _ = $0 }))
                            .listRowBackground(self.colorScheme == .dark ? Color.black : Color.white)
                    }
                    .onDelete(perform: self.removeAllergy)
                }
            } else {
                Spacer()
            }
        }
        .onAppear(perform: {
            self.allergies = self.allergyService.fetchProfileAllergies(profile: self.profile) ?? []
         })
        .sheet(isPresented: self.$addingAllergy, onDismiss: {
            self.addingAllergy = false
            _ = self.allergyService.saveChanges()
            self.allergies = self.allergyService.fetchProfileAllergies(profile: self.profile) ?? []
        }, content: {
            AddAllergy(profile: .constant(self.profile), showing: self.$addingAllergy, currentAllergies: self.$allergies.wrappedValue)
                .environmentObject(self.allergyService)
        })
    }

    private func removeAllergy(at offsets: IndexSet) {
        for index in offsets {
            self.allergyService.removeAllergy(allergy: self.allergies[index])
            _ = self.allergyService.saveChanges()
            self.allergies.remove(at: index)
        }
    }
}

struct AllergiesList_Previews: PreviewProvider {
    static var previews: some View {
        AllergiesList(profile: .constant(ProfileRecord()), allergies: .constant([]))
    }
}
