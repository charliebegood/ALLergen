//
//  CustomPage.swift
//  ALLergen
//
//  Created by Charles AUBERT on 1/11/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import SwiftUI

struct CustomPage<Content>: View where Content: View {

    let content: () -> Content

    init(@ViewBuilder content: @escaping () -> Content) {
        self.content = content
    }

    var body: some View {
        content()
    }

}

struct CustomPage_Previews: PreviewProvider {
    static var previews: some View {
        CustomPage() {
            Text("Hi")
        }
    }
}

