//
//  ProductCard.swift
//  ALLergen
//
//  Created by Charles AUBERT on 1/13/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import SwiftUI

public struct SwippableCard<Content> : View where Content : View {
    @Binding var defaultPosition : CardPosition
    @Binding var maxPosition : CardPosition
    @Binding var backgroundStyle: BackgroundStyle
    var content: () -> Content
    var bottomOffset: CGFloat = 0

    public init(_ position: Binding<CardPosition> = .constant(.middle), _ maxPosition: Binding<CardPosition>, backgroundStyle: Binding<BackgroundStyle> = .constant(.solid), bottomOffset: CGFloat, content: @escaping () -> Content) {
        self.content = content
        self._defaultPosition = position
        self._backgroundStyle = backgroundStyle
        self._maxPosition = maxPosition
        self.bottomOffset = bottomOffset
    }

    public var body: some View {
        ModifiedContent(content: self.content(), modifier: Card(position: self.$defaultPosition, backgroundStyle: self.$backgroundStyle, maxPosition: self.$maxPosition, bottomOffset: self.bottomOffset))
    }
}

public enum BackgroundStyle {
    case solid, clear, blur
}

public enum CardPosition: CGFloat {
    case bottom , middle, top

    func offsetFromTop(bottomOffset: CGFloat) -> CGFloat {
        switch self {
        case .bottom:
            return UIScreen.main.bounds.height
        case .middle:
            return UIScreen.main.bounds.height - 180 - bottomOffset
        case .top:
            return 65
        }
    }
}


enum DragState {
    case inactive
    case dragging(translation: CGSize)

    var translation: CGSize {
        switch self {
        case .inactive:
            return .zero
        case .dragging(let translation):
            return translation
        }
    }

    var isDragging: Bool {
        switch self {
        case .inactive:
            return false
        case .dragging:
            return true
        }
    }
}

struct Card: ViewModifier {
    @Environment(\.colorScheme) var colorScheme: ColorScheme
    @GestureState var dragState: DragState = .inactive
    @Binding var position: CardPosition
    @Binding var backgroundStyle: BackgroundStyle
    @State var offset: CGSize = CGSize.zero
    @Binding var maxPosition: CardPosition
    var bottomOffset: CGFloat

    init(position: Binding<CardPosition>, backgroundStyle: Binding<BackgroundStyle>, maxPosition: Binding<CardPosition>, bottomOffset: CGFloat) {
        _position = position
        _backgroundStyle = backgroundStyle
        _maxPosition = maxPosition
        self.bottomOffset = bottomOffset
    }

    var animation: Animation {
        Animation.interpolatingSpring(stiffness: 300.0, damping: 30.0, initialVelocity: 10.0)
    }

    var timer: Timer? {
        return Timer.scheduledTimer(withTimeInterval: 1.0, repeats: false) { timer in
            if self.position == .top && self.dragState.translation.height == 0  {
                self.position = .top
            } else {
                timer.invalidate()
            }
        }
    }

    func body(content: Content) -> some View {
        let drag = DragGesture()
            .updating($dragState) { drag, state, transaction in
                if (self.position == self.maxPosition && drag.translation.height < 0) {
                    return
                }
                state = .dragging(translation:  drag.translation)
            }
            .onChanged {_ in
                self.offset = .zero
            }
            .onEnded(onDragEnded)

        return ZStack(alignment: .top) {
            ZStack(alignment: .top) {

                if backgroundStyle == .blur {
                    BlurView(style: colorScheme == .dark ? .constant(.dark) : .constant(.extraLight))
                }

                if backgroundStyle == .clear {
                    Color.clear
                }

                if backgroundStyle == .solid {
                    colorScheme == .dark ? Color.black : Color.white
                }
                VStack() {
                    Rectangle().foregroundColor(.clear).background(Color(colorScheme == .dark ? .black : .white))
                }
                .padding(.top, 165 + 15)

                Handle()

                content
                    .padding(.top, 15)
            }
            .mask(RoundedRectangle(cornerRadius: 20, style: .continuous))
            .scaleEffect(x: 1, y: 1, anchor: .center)
        }
        .offset(y:  max(0, self.position.offsetFromTop(bottomOffset: self.bottomOffset) + self.dragState.translation.height))
        .animation((self.dragState.isDragging ? nil : animation))
        .gesture(drag)
    }

    private func onDragEnded(drag: DragGesture.Value) {

        // Setting stops
        let higherStop: CardPosition
        let lowerStop: CardPosition

        // Nearest position for drawer to snap to.
        let nearestPosition: CardPosition

        // Determining the direction of the drag gesture and its distance from the top
        let dragDirection = drag.predictedEndLocation.y - drag.location.y
        let offsetFromTopOfView = position.offsetFromTop(bottomOffset: self.bottomOffset) + drag.translation.height

        // Determining whether drawer is above or below `.partiallyRevealed` threshold for snapping behavior.
        if offsetFromTopOfView <= CardPosition.middle.offsetFromTop(bottomOffset: self.bottomOffset) {
            higherStop = .top
            lowerStop = .middle
        } else {
            higherStop = .middle
            lowerStop = .bottom
        }

        if (self.maxPosition == lowerStop) {
            return
        }
        // Determining whether drawer is closest to top or bottom
        if (offsetFromTopOfView - higherStop.offsetFromTop(bottomOffset: self.bottomOffset)) < (lowerStop.offsetFromTop(bottomOffset: self.bottomOffset) - offsetFromTopOfView) {
            nearestPosition = higherStop
        } else {
            nearestPosition = lowerStop
        }

        // Determining the drawer's position.
        if dragDirection > 0 {
            position = lowerStop
        } else if dragDirection < 0 {
            position = higherStop
        } else {
            position = nearestPosition
        }
        _ = timer
    }
}

struct Handle : View {
    private let handleThickness = CGFloat(5.0)
    var body: some View {
        RoundedRectangle(cornerRadius: handleThickness / 2.0)
            .frame(width: 40, height: handleThickness)
            .foregroundColor(Color.secondary)
            .padding(5)
    }
}
