//
//  LoadingLabel.swift
//  ALLergen
//
//  Created by Charles AUBERT on 4/27/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import SwiftUI

struct LoadingLabel: View {
    @Binding var loading: Bool

    var body: some View {
        HStack(alignment: .center) {
            Spacer()

            Text("Loading")
                .font(.system(size: 20, weight: .medium, design: .default))
                .foregroundColor(.systemGray)

            ActivityIndicator(isAnimating: self.$loading, style: .medium)
                .frame(width: 20, height: 20)

            Spacer()
        }
    }
}

struct LoadingLabel_Previews: PreviewProvider {
    static var previews: some View {
        LoadingLabel(loading: .constant(true))
    }
}
