//
//  StackViewManager.swift
//  ALLergen
//
//  Created by Charles AUBERT on 1/20/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import SwiftUI
import Combine

class CardsManager: ObservableObject {
    @Published var positions: ViewPositions = ViewPositions()
    @Published var position: ViewPosition = .center
    @Published var animating: Bool = false
    @Published var shadow: Double = 0
    @Published var scale: CGFloat = 1
    var initalDirection: DragDirection = .none

    func updateOffset(value: DragGesture.Value) {
        var offset = CGPoint(x: value.translation.width, y: value.translation.height)
        let direction: DragDirection = offset.x < 0 ? .left : .right

        self.animating = false
        if (self.initalDirection == .none) {
            self.initalDirection = direction
        }
        if (self.position == .left) {
            let transformation = (UIScreen.main.bounds.width - abs(self.positions.left)) / UIScreen.main.bounds.width
            self.positions.left = min(0, offset.x)
            self.shadow = Double(transformation)
            self.scale = 1 - transformation / 8
        } else if (self.position == .right) {
            let transformation = 1 - self.positions.right / UIScreen.main.bounds.width
            self.shadow = Double(transformation)
            self.positions.right = max(0, offset.x)
            self.scale = 1 - transformation / 8
        } else if (self.initalDirection == .left) {
            if (direction == .right) {
                offset.x = min(0, offset.x)
            }
            self.positions.right = ViewPosition.getViewStartOffset(.right) + offset.x
            let transformation = 1 - self.positions.right / UIScreen.main.bounds.width
            self.shadow = min(1, Double(transformation))
            self.scale = 1 - ((1 - self.positions.right / UIScreen.main.bounds.width) / 8)
        } else if (self.initalDirection == .right) {
            if (direction == .left) {
                offset.x = max(0, offset.x)
            }
            self.positions.left = ViewPosition.getViewStartOffset(.left) + offset.x
            let transformation = (UIScreen.main.bounds.width - abs(self.positions.left)) / UIScreen.main.bounds.width
            self.shadow = min(1, Double(transformation))
            self.scale = 1 - (((UIScreen.main.bounds.width - abs(self.positions.left)) / UIScreen.main.bounds.width) / 8)
        }
    }

    func calculateFinalPositions(value: DragGesture.Value) {
        let offset = CGPoint(x: value.translation.width, y: value.translation.height)
        let prediction = value.predictedEndLocation
        let translation = prediction.x - offset.x

        if (self.position == .left && initalDirection == .left) {
            if (translation <= 100 || offset.x <= ViewPosition.getViewStartOffset(.left) / 2) {
                self.changePosition(newPosition: .center, animate: false)
            } else {
                self.setPosition(position: .left, start: false)
            }
        } else if (self.position == .right && initalDirection == .right) {
            if (translation >= 200 || offset.x >= ViewPosition.getViewStartOffset(.right) / 2) {
                self.changePosition(newPosition: .center, animate: false)
            } else {
                self.setPosition(position: .right, start: false)
            }
        } else {
            if (self.initalDirection == .right) {
                if (translation >= 200 || offset.x >= ViewPosition.getViewStartOffset(.right) / 2) {
                    self.changePosition(newPosition: .left, animate: false)
                } else {
                    self.setPosition(position: .left, start: true)
                }
            } else if (self.initalDirection == .left) {
                if (translation <= 200 || offset.x <= ViewPosition.getViewStartOffset(.left) / 2) {
                    self.changePosition(newPosition: .right, animate: false)
                } else {
                    self.setPosition(position: .right, start: true)
                }
            }
        }
        self.initalDirection = .none
        self.animating = true
    }

    func setPosition(position: ViewPosition, start: Bool) {
        if (position == .left) {
            self.positions.left = start ? ViewPosition.getViewStartOffset(position) : ViewPosition.getViewEndOffset(position)
        } else {
            self.positions.right = start ? ViewPosition.getViewStartOffset(position) : ViewPosition.getViewEndOffset(position)
        }
        if (start) {
            self.shadow = 0
            self.scale = 1
        } else {
            self.shadow = 1
            self.scale = 0.8
        }
    }

    func changePosition(newPosition: ViewPosition, animate: Bool) {
        if (self.position == newPosition) {
            return
        }
        self.animating = animate
        if (self.position == .center) {
            self.setPosition(position: newPosition, start: false)
        } else if (self.position == .left && newPosition == .center) {
            self.setPosition(position: .left, start: true)
        } else if (self.position == .right && newPosition == .center) {
            self.setPosition(position: .right, start: true)
        }
        self.position = newPosition
    }

    var animation: Animation {
        Animation.interpolatingSpring(mass: 0.9, stiffness: 400.0, damping: 50.0, initialVelocity: 5)
    }
}

struct StackManagerView<LeftPage: CardStackView, CenterPage: CardStackView, RightPage: CardStackView>: View {
    @ObservedObject var cardsManager: CardsManager
    var leftPage: LeftPage
    var centerPage: CenterPage
    var rightPage: RightPage

    init(left: LeftPage.Type, center: CenterPage.Type, right: RightPage.Type) {
        let manager = CardsManager()
        self.leftPage = left.init(manager: manager)
        self.centerPage = center.init(manager: manager)
        self.rightPage = right.init(manager: manager)
        self._cardsManager = ObservedObject(initialValue: manager)
    }

    var body: some View {
        GeometryReader { geometry in
            ZStack() {
//                ZStack() {
//                    self.centerPage
//                        .cornerRadius(40)
//                        .scaleEffect(self.cardsManager.scale)
//                        .animation(self.cardsManager.animating ? Animation.easeOut(duration: 0.3) : nil)
//                    Rectangle().foregroundColor(Color.black)
//                        .opacity(self.cardsManager.shadow)
//                        .animation(self.cardsManager.animating ? Animation.easeOut(duration: 0.5) : nil)
//                }
//                self.leftPage
//                    .cornerRadius(40)
//                    .offset(x: self.cardsManager.positions.left)
//                    .animation(self.cardsManager.animating ? self.cardsManager.animation : nil)
                self.rightPage
//                    .cornerRadius(40)
//                    .offset(x: self.cardsManager.positions.right)
//                    .animation(self.cardsManager.animating ? self.cardsManager.animation : nil)
            }
            .gesture(DragGesture()
                .onChanged { value in
                    self.cardsManager.updateOffset(value: value)
                }
                .onEnded { value in
                    self.cardsManager.calculateFinalPositions(value: value)
                }
            )
            .background(Color.black)
        }
        .onAppear(perform: {
            self.cardsManager.positions.left = ViewPosition.getViewStartOffset(.left)
            self.cardsManager.positions.right = ViewPosition.getViewStartOffset(.right)
        })
    }
}

enum DragDirection {
    case none, left, right
}

struct ViewPositions {
    var left = ViewPosition.getViewEndOffset(.left)
    var center = ViewPosition.getViewStartOffset(.center)
    var right = ViewPosition.getViewEndOffset(.right)
}

enum ViewPosition {
    case left, center, right

    static func getViewStartOffset(_ position: ViewPosition) -> CGFloat {
        switch position {
        case .left:
            return -UIScreen.main.bounds.width
        case .center:
            return 0
        case .right:
            return UIScreen.main.bounds.width
        }
    }

    static func getViewEndOffset(_ position: ViewPosition) -> CGFloat {
        switch position {
        case .left:
            return 0
        case .center:
            return 0
        case .right:
            return 0
        }
    }
}

protocol CardStackView: View {
    init(manager: CardsManager)
    var manager: CardsManager { get set }
}
