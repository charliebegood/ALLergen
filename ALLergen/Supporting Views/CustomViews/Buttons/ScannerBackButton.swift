//
//  BackButton.swift
//  ALLergen
//
//  Created by Charles AUBERT on 4/13/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import SwiftUI

enum ButtonDirection {
    case right, left
}

struct ScannerBackButton: View {
    @State var direction: ButtonDirection
    var action: () -> Void

    var body: some View {
        Button(action: action) {
            if (self.direction == .left) {
                HStack() {
                    Image(systemName: "arrowtriangle.left.fill")
                        .font(.system(size: 20, weight: .medium, design: .default))
                        .foregroundColor(.white)
                    Text("Scanner")
                        .foregroundColor(.white)
                        .font(.system(size: 20, weight: .medium, design: .default))
                }
            } else {
                HStack() {
                    Text("Scanner")
                        .foregroundColor(.white)
                        .font(.system(size: 20, weight: .medium, design: .default))
                    Image(systemName: "arrowtriangle.right.fill")
                        .font(.system(size: 20, weight: .medium, design: .default))
                        .foregroundColor(.white)
                }
            }
        }
    }
}

struct ScannerBackButton_Previews: PreviewProvider {
    static var previews: some View {
        ScannerBackButton(direction: .left, action: { print("Back to scanner") })
    }
}
