//
//  Styles.swift
//  ALLergen
//
//  Created by Charles AUBERT on 4/16/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import SwiftUI

struct CancelButtonStyle: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.system(size: 17, weight: .regular, design: .default))
            .foregroundColor(Color("Flame"))
    }
}

struct DoneButtonStyle: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.system(size: 20, weight: .semibold, design: .default))
            .foregroundColor(Color("Spring Green"))
    }
}

struct DisabledButtonStyle: ViewModifier {
    func body(content: Content) -> some View {
        content
            .font(.system(size: 17, weight: .regular, design: .default))
            .foregroundColor(.systemGray4)
    }
}

extension Text {
    func textStyle<Style: ViewModifier>(_ style: Style) -> some View {
        ModifiedContent(content: self, modifier: style)
    }
}
