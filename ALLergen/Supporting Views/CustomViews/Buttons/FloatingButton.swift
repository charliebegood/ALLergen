//
//  FloatingButton.swift
//  ALLergen
//
//  Created by Charles AUBERT on 12/4/19.
//  Copyright © 2019 Charles AUBERT. All rights reserved.
//

import SwiftUI

struct FloatingButtonLabel: View {
    var title: String

    var body: some View {
        Image(systemName: title)
            .font(.system(size: 17, weight: .medium, design: .default))
    }
}

struct FloatingButton: View {
    var title: String = ""
    var size: CGFloat = 55
    var selectedTitle: String = ""
    var selectable: Bool = false
    @Binding var selected: Bool

    var body: some View {
        Button(action: action) {
            FloatingButtonLabel(title: self.selectable && self.selected ? self.selectedTitle : self.title)
        }
        .frame(width: size, height: size, alignment: .center)
        .buttonStyle(FloatingButtonStyle(selected: self.$selected))
    }
    var action: () -> Void = {}
}

struct FloatingButtonStyle: ButtonStyle {
    @Binding var selected: Bool

    func makeBody(configuration: Self.Configuration) -> some View {
        GeometryReader { geometry in
            configuration.label
                .frame(minWidth: 30, maxWidth: .infinity, minHeight:30, maxHeight: .infinity)
                .background (
                    BlurView(style: self.selected ? .constant(.light) : .constant(.dark))
                        .cornerRadius(geometry.size.width)
                )
                .font(.system(size: 45, weight: .medium, design: .default))
                .foregroundColor(configuration.isPressed ? .gray : .white)
                .scaleEffect(configuration.isPressed ? 0.9 : 1.0)
        }
    }
}

struct FloatingButton_Previews: PreviewProvider {
    static var previews: some View {
        FloatingButton(title: "flashlight.off.fill", size: 60, selectable: true, selected: .constant(false), action: {
            print("Hello")
        })
    }
}
