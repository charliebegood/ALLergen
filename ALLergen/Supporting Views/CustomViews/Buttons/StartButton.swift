//
//  StartButton.swift
//  ALLergen
//
//  Created by Charles AUBERT on 4/16/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import SwiftUI

struct StartButton: View {
    var action: () -> Void = {}
    var title: String
    var invertedColor: Bool = false

    var body: some View {
        Button(action:action) {
            Text(self.title)
        }
        .buttonStyle(StartButtonStyle(inverted: invertedColor))
    }

}

struct StartButtonStyle: ButtonStyle {
    var inverted: Bool

    func makeBody(configuration: Self.Configuration) -> some View {
        configuration.label
            .padding([.top, .bottom], 10)
            .padding([.leading, .trailing], 30)
            .background(inverted ? Color.white : Color("Dark Green"))
            .cornerRadius(10)
            .font(.system(size: 20, weight: .semibold, design: .rounded))
            .foregroundColor(inverted ? Color("Dark Green") : .white)
    }
}

struct StartButton_Previews: PreviewProvider {
    static var previews: some View {
        StartButton(action: {print("Hi")}, title: "Add 􀁌")
    }
}
