//
//  ProductModal.swift
//  ALLergen
//
//  Created by Charles AUBERT on 1/15/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import SwiftUI
import ObjectMapper
import SDWebImageSwiftUI

struct ProductModal: View {
    @EnvironmentObject var analyzeService: AnalyzeService
    @EnvironmentObject var productService: ProductService
    @Environment(\.colorScheme) var colorScheme: ColorScheme

    @State var product: Product
    @State var profiles: [String: [ProfileRecord]] = ["": []]
    @State var analyzing: Bool = false
    @State var loading: Bool = false

    var body: some View {
        GeometryReader { geometry in
            ZStack(alignment: .top) {
                VStack(alignment: .leading, spacing: 0) {
                    ZStack() {
                        WebImage(url: URL(string: self.product.frontImage ?? ""))
                            .placeholder(Image(systemName: "photo"))
                            .resizable()
                            .scaledToFill()
                            .frame(width: geometry.size.width, height: 165 + 15)

                        BlurView(style: .constant(self.colorScheme == .dark ? .dark : .extraLight))
                            .frame(width: geometry.size.width, height: 165 + 15)
                    }

                    Rectangle()
                        .foregroundColor(.clear)
                        .background(Color(self.colorScheme == .dark ? .black : .white))
                }

                VStack(alignment: .center, spacing: 0) {
                    HStack {
                        Spacer()

                        Handle()

                        Spacer()
                    }

                    ProductCardHeader(product: self.$product, profiles: self.$profiles, analyzing: self.$analyzing, loading: self.$loading)

                    if (self.loading) {
                        Spacer()

                        LoadingLabel(loading: self.$loading)
                    } else {
                        ProductCardBody(product: self.product, profiles: self.$profiles)
                    }

                    Spacer()
                }
                .padding(.top, 5)
            }
            .onAppear(perform: {
                self.loadProduct()
            })
        }
    }

    func loadProduct() {
        self.loading = true
        self.productService.getProductFromBarcode(barcode: self.product.code ?? "", preview: true, onSuccess: {(productResponse: ProductResponse) in
            self.loading = false
            if let product = productResponse.product {
                self.product = product
                if (self.product.validate()) {
                    self.translateProduct()
                }
            } else {
                self.product.valid = false
            }
        }, onError: {(error: Error) in
            print(error)
            self.loading = false
        })
    }

    func translateProduct() {
        self.analyzing = true
        self.productService.getTranslatedIngredients(ingredients: product.ingredients ?? "", onSuccess: {(translationResponse: TranslationResponse) in
            self.product.ingredients = translationResponse.text?[0]
            self.analyzeProduct()
        }, onError: {(error: Error) in
            print(error)
            self.analyzing = false
            self.product.valid = false
        })
    }

    func analyzeProduct() {
        self.profiles = self.analyzeService.analyzeIngredients(ingredients: self.product.ingredients ?? "")
        self.analyzing = false
    }
}
