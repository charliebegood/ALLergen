//
//  CardHeader.swift
//  ALLergen
//
//  Created by Charles AUBERT on 1/13/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI

struct ProductCardHeader: View {
    @Environment(\.colorScheme) var colorScheme: ColorScheme
    @EnvironmentObject var profileService: ProfileService

    @Binding var product: Product
    @Binding var profiles: [String: [ProfileRecord]]
    @Binding var analyzing: Bool
    @Binding var loading: Bool

    var body: some View {
        HStack(alignment: .center, spacing: 15) {
            ZStack() {
                Rectangle()
                    .foregroundColor(.clear)

                WebImage(url: URL(string: self.product.frontImage ?? ""))
                    .placeholder(Image(systemName: "photo"))
                    .resizable()
                    .scaledToFit()
                    .cornerRadius(10)
            }
            .frame(width: 115, height: 115)

            VStack(alignment: .leading, spacing: 5) {
                Text(product.brands ?? "")
                    .font(.system(size: 20, weight: .medium, design: .default))
                    .foregroundColor(.systemGray)

                Text(product.name ?? "")
                    .font(.system(size: 20, weight: .bold, design: .default))
                    .lineLimit(2)

                if (analyzing) {
                    HStack(alignment: .center) {
                        Text("Analyzing")
                            .font(.system(size: 20, weight: .medium, design: .default))
                            .foregroundColor(.systemGray)
                        ActivityIndicator(isAnimating: self.$analyzing, style: .medium)
                            .frame(width: 20, height: 20)
                    }
                } else {
                    if (profiles["unsafe"]?.count ?? 0 > 0) {
                        HStack(alignment: .center) {
                            Text("Allergies")
                                .font(.system(size: 20, weight: .medium, design: .default))
                                .foregroundColor(Color("Flame"))
                            Image(systemName: "exclamationmark.triangle.fill")
                                .foregroundColor(Color("Flame"))
                        }
                    } else if (profiles["safe"]?.count ?? 0 == self.profileService.profiles.count && self.profileService.profiles.count > 0) {
                        HStack(alignment: .center) {
                            Text("Safe")
                                .font(.system(size: 20, weight: .medium, design: .default))
                                .foregroundColor(Color("Light Green"))
                            Image(systemName: "checkmark.circle.fill")
                                .foregroundColor(Color("Light Green"))
                        }
                    }
                }
            }
        }
        .padding(16)
        .frame(height: 165, alignment: .top)
    }
}

struct NotFoundCardHeader: View {
    @Environment(\.colorScheme) var colorScheme: ColorScheme
    @EnvironmentObject var ocrService: OCRService

    var body: some View {
        VStack() {
            HStack() {
                Spacer()
                Image(systemName: "exclamationmark.triangle.fill")
                    .foregroundColor(Color(colorScheme == .dark ? .white : .black))
                Text("No product found")
                    .font(.system(size: 22, weight: .medium, design: .default))


                Spacer()
            }
            .frame(alignment: .center)

            Spacer().frame(height: 10)

            Button(action: {
                self.ocrService.showCamera.toggle()
            }) {
                Text("Scan Ingredients")
                    .textStyle(DoneButtonStyle())
            }
        }
        .padding(16)
        .frame(height: 150, alignment: .center)
    }
}

struct LoadingCardHeader: View {
    @Environment(\.colorScheme) var colorScheme: ColorScheme
    @Binding var cancellable: Bool
    var cancelHandler: () -> Void = {}

    var body: some View {
        VStack {
            LoadingLabel(loading: .constant(true))
            if (self.cancellable) {
                Button(action: self.cancelHandler) {
                    Text("Cancel")
                        .textStyle(CancelButtonStyle())
                }
            }
        }
        .padding(16)
        .frame(height: 150, alignment: .center)
    }
}
