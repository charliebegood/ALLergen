//
//  ProductCard.swift
//  ALLergen
//
//  Created by Charles AUBERT on 4/25/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import SwiftUI

struct ProductCard: View {
    @Binding var product: Product
    @Binding var productCardPosition: CardPosition
    @Binding var maxProductCardPosition: CardPosition
    @Binding var loading: Bool

    @State var profiles: [String: [ProfileRecord]] = ["": []]
    @State var analyzing: Bool = false

    @EnvironmentObject var analyzeService: AnalyzeService
    @EnvironmentObject var productService: ProductService
    @EnvironmentObject var ocrService: OCRService

    var body: some View {
        GeometryReader { geometry in
            SwippableCard(self.$productCardPosition, self.$maxProductCardPosition, backgroundStyle: .constant(.blur), bottomOffset: max(0, UIScreen.main.bounds.height - geometry.size.height)) {

                VStack(alignment: .leading, spacing: 0) {
                    if (self.loading) {
                        LoadingCardHeader(cancellable: Binding(get: { self.ocrService.loading }, set: { _ = $0 }), cancelHandler: {
                            self.ocrService.cancelTasks()
                            self.loading = false
                            self.maxProductCardPosition = self.product.validate() ? .top : .middle
                        })
                    } else if (self.product.valid == true) {
                        ProductCardHeader(product: self.$product, profiles: self.$profiles, analyzing: self.$analyzing, loading: self.$loading)

                        Spacer().frame(height: 20)
                        ProductCardBody(product: self.product, profiles: self.$profiles)
                        Spacer().frame(height: UIScreen.main.bounds.height - geometry.size.height)
                    } else {
                        NotFoundCardHeader()
                    }
                }
            }
            .edgesIgnoringSafeArea(.bottom)

        }
        .sheet(isPresented: Binding(get: { self.ocrService.showCamera }, set: { self.ocrService.showCamera = $0 }), onDismiss: {
            self.ocrService.showCamera = false
            self.product.reset()
            self.loading = true
            self.product.name = "Scanned Item"
            self.product.brands = "Unkown"
            if self.ocrService.capturedImage != nil {
                self.ocrService.analyzeImage(image: self.ocrService.capturedImage!, completionHandler: { result in
                    self.loading = false
                    self.product.ingredients = result
                    self.maxProductCardPosition = self.product.validate() ? .top : .middle
                    self.translateProduct()
                })
            }
        }) {
            OCRView(image: Binding(get: { self.ocrService.capturedImage }, set: { self.ocrService.capturedImage = $0 }) , showCaptureImageView: Binding(get: { self.ocrService.showCamera }, set: { self.ocrService.showCamera = $0 }))
        }
        .onReceive(product.$valid, perform: { valid in
            if (valid) {
                self.translateProduct()
            }
        })
    }

    func translateProduct() {
        self.profiles = ["": []]
        self.analyzing = true
        self.productService.getTranslatedIngredients(ingredients: product.ingredients ?? "", onSuccess: {(translationResponse: TranslationResponse) in
            self.product.ingredients = translationResponse.text?[0]
            self.analyzeProduct()
        }, onError: {(error: Error) in
            print(error)
            self.product.valid = false
            self.maxProductCardPosition = self.product.validate() ? .top : .middle
        })
    }

    func analyzeProduct() {
        self.profiles = self.analyzeService.analyzeIngredients(ingredients: self.product.ingredients ?? "")
        self.analyzing = false
    }
}
