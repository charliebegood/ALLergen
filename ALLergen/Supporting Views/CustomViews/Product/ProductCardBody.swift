//
//  CardBody.swift
//  ALLergen
//
//  Created by Charles AUBERT on 1/13/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import SwiftUI

struct ProductCardBody: View {
    @Environment(\.colorScheme) var colorScheme: ColorScheme

    @ObservedObject var product: Product
    @Binding var profiles: [String: [ProfileRecord]]

    var body: some View {
        ScrollView {
            VStack(alignment: .leading, spacing: 25) {
                SafetyList(profiles: self.$profiles)

                HStack {
                    if (self.product.ingredients ?? "" != "") {
                        VStack(alignment: .leading, spacing: 8) {
                             Text("Ingredients")
                                 .font(.system(size: 20, weight: .bold, design: .default))

                             Text(self.product.ingredients ?? "")
                                 .font(.system(size: 15, weight: .medium, design: .default))
                                 .foregroundColor(Color(red: 153/255, green: 153/255, blue: 153/255))
                        }
                    } else {
                        Text("No information available.")
                            .font(.system(size: 20, weight: .bold, design: .default))
                            .padding(16)
                    }
                    Spacer()
                }

                Spacer()
            }
            .padding([.leading, .trailing], 20)
        }
    }
}
