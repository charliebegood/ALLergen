//
//  SearchResultRow.swift
//  ALLergen
//
//  Created by Charles AUBERT on 1/15/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import SwiftUI
import SDWebImageSwiftUI

struct SearchResultRow: View {
    var product: Product

    var body: some View {
        HStack(alignment: .center, spacing: 15) {
            ZStack() {
                Rectangle()
                    .foregroundColor(.clear)

                WebImage(url: URL(string: self.product.frontImage ?? ""))
                    .placeholder(Image(systemName: "photo"))
                    .resizable()
                    .scaledToFit()
                    .cornerRadius(5)
            }
            .frame(maxWidth: 60, maxHeight: 60, alignment: .leading)

            VStack(alignment: .leading, spacing: 5) {
                Text(product.name ?? "")
                    .font(.system(size: 16, weight: .semibold, design: .default))
                    .lineLimit(1)

                Text(product.brands ?? "")
                    .font(.system(size: 14, weight: .regular, design: .default))
                    .foregroundColor(Color(red: 153/255, green: 153/255, blue: 153/255))
            }
            Spacer()
        }
        .padding([.top, .bottom], 8)
        .frame(height: 70, alignment: .top)
    }
}
