//
//  SearchResult.swift
//  ALLergen
//
//  Created by Charles AUBERT on 1/15/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import SwiftUI

struct SearchBodySuggestions: View {
    @EnvironmentObject var searchService: SearchService

    @State private var recentSearches: [SearchSuggestion] = [SearchSuggestion]()

    @Binding var searchBarStateManager: SearchBarStateManager
    @Binding var page: Int

    var body: some View {
        VStack(alignment: .leading, spacing: 10) {
            Text("Recent")
                .font(.system(size: 22, weight: .bold, design: .default))

            ForEach(self.recentSearches, id: \.self) { (search: SearchSuggestion) in
                VStack(alignment: .leading, spacing: 10) {
                    Divider()

                    Text(search.title ?? "")
                        .font(.system(size: 17, weight: .medium, design: .default))
                        .foregroundColor(Color("Light Green"))
                }
                .onTapGesture(perform: {
                    self.searchBarStateManager.changeState(newState: .typing)
                    self.searchBarStateManager.textInput = search.title ?? ""
                })
            }
        }
        .padding(.top, 5)
        .padding([.leading, .trailing], 20)
        .onAppear(perform: {
            DispatchQueue.main.async(execute: {
                self.recentSearches = self.searchService.getRecentQueries(length: 10)
            })
        })
    }
}

struct SearchBodyResults: View {
    @EnvironmentObject var productService: ProductService
    @EnvironmentObject var searchService: SearchService
    @EnvironmentObject var profileService: ProfileService
    @EnvironmentObject var analyzeService: AnalyzeService

    @ObservedObject var searchResponse: SearchResponse

    @Binding var searchBarStateManager: SearchBarStateManager
    @Binding var page: Int

    @State var showProduct: Bool = false
    @State var selectedProduct: Product = Product(JSON: ["":""])!
    @State var name: String = ""
    @State var loading: Bool = false

    var pageSize: Int = 20

    var body: some View {
        List {
            ForEach(self.searchResponse.products ?? [], id: \.self) { product in
                SearchResultRow(product:product)
                    .gesture(TapGesture().onEnded { _ in
                        self.showProduct = true
                        self.selectedProduct = product
                        self.searchBarStateManager.changeState(newState: .validated)
                    })
            }

            HStack(alignment: .center) {
                Spacer()

                Text(loading ? "Loading..." : String(self.searchResponse.products?.count ?? 0) + " items")
                    .font(.system(size: 20, weight: .medium, design: .default))
                    .foregroundColor(.systemGray)

                if (loading) {
                    ActivityIndicator(isAnimating: self.$loading, style: .medium)
                        .frame(width: 20, height: 20)
                }

                Spacer()
            }
            .onAppear(perform: self.loadNewPage)
        }
        .sheet(isPresented: self.$showProduct, onDismiss: {
            self.showProduct = false
        }, content: {
            ProductModal(product: self.selectedProduct)
                .environmentObject(self.productService)
                .environmentObject(self.profileService)
                .environmentObject(self.analyzeService)
        })
    }

    func loadNewPage() {
        if ((self.searchResponse.products?.count ?? 0) == self.pageSize * self.page && (self.searchResponse.products?.count ?? 0) < (self.searchResponse.count ?? 0)) {
            self.loading = true
            self.page += 1
            self.searchService.getNewPage(query: self.searchBarStateManager.textInput, page: self.page, onSuccess: {(response: SearchResponse) in
                self.loading = false
                self.searchResponse.products?.append(contentsOf: response.products ?? [])
            }, onError: {(error: Error) in
                self.loading = false
                print(error)
            })
        }
    }
}

struct SearchBody: View {
    @EnvironmentObject var searchService: SearchService

    @Binding var searchBarStateManager: SearchBarStateManager

    @State var searchResponse: SearchResponse = SearchResponse(JSON: ["": ""])!
    @State var page: Int = 1
    @State var loading: Bool = false

    var body: some View {
        VStack {
            if (loading) {
                LoadingLabel(loading: self.$loading)

                Spacer()
            } else if (self.searchBarStateManager.state != .inactive && self.searchBarStateManager.state != .canceled && searchResponse.products?.count ?? 0 > 0) {
                SearchBodyResults(searchResponse: self.searchResponse, searchBarStateManager: self.$searchBarStateManager, page: self.$page)
            } else {
                SearchBodySuggestions(searchBarStateManager: self.$searchBarStateManager, page: self.$page)

                Spacer()
            }
        }
        .onReceive(self.searchBarStateManager.stateChanged, perform: self.updateSearchState(newState:))
    }

    func updateSearchState(newState: SearchBarState) {
        if (newState == .typing && self.searchBarStateManager.textInput != "") {
            self.loading = true
            self.searchService.getSearchResults(query: self.searchBarStateManager.textInput, onSuccess: {(response: SearchResponse) in
                self.searchResponse = response
                self.loading = false
            }, onError: {(error: Error) in
                print(error)
                self.loading = false
            })
        } else if (newState == .validated) {
            self.searchService.saveNewQuery(title: self.searchBarStateManager.textInput)
        } else if (newState == .canceled) {
            self.searchResponse.products = []
        }
    }
}
