//
//  ProfilePicture.swift
//  ALLergen
//
//  Created by Charles AUBERT on 4/23/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import SwiftUI

struct ProfilePicture: View {
    @Environment(\.colorScheme) var colorScheme: ColorScheme
    @Binding var editing: Bool
    @Binding var name: String
    @Binding var picture: UIImage?

    var size: PictureSize = .normal

    init(name: Binding<String>, picture: Binding<UIImage?>, editing: Binding<Bool>, size: PictureSize = .normal) {
        _name = name
        _picture = picture
        _editing = editing
        self.size = size
    }

    var body: some View {
        ZStack {
            if (self.picture != nil) {
                Image(uiImage: self.picture!)
                    .renderingMode(.original)
                    .resizable()
                    .clipShape(Circle())
                    .frame(width: self.size.getPictureSize(), height: self.size.getPictureSize())
            } else {
                Circle()
                    .frame(width: self.size.getPictureSize(), height: self.size.getPictureSize())
                    .foregroundColor(self.size == .big ? self.colorScheme == .dark ? .systemGray3 : .white : self.colorScheme == .dark ? .systemGray3 : .systemGray6)
                if (self.editing) {
                    Image(systemName: "camera.circle.fill")
                        .resizable()
                        .frame(width: 40 , height: 40)
                        .foregroundColor(self.colorScheme == .dark ? .white : .black)
                } else {
                    Text(self.name.prefix(1))
                        .font(.system(size: self.size == .big ? 40 : self.size == .normal ? 20 : 15, weight: .bold, design: .rounded))
                        .foregroundColor(self.colorScheme == .dark ? .white : .black)
                }
            }
        }
    }
}

enum PictureSize {
    case big, normal, small

    func getPictureSize() -> CGFloat {
        switch self {
        case .big:
            return 100
        case .normal:
            return 50
        case .small:
            return 30
        }
    }
}
