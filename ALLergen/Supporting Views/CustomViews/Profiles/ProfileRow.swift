//
//  ProfileRow.swift
//  ALLergen
//
//  Created by Charles AUBERT on 4/16/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import SwiftUI

struct ProfileRow: View {
    @EnvironmentObject var allergyService: AllergyService
    @Environment(\.colorScheme) var colorScheme: ColorScheme

    @State var allergies: [AllergyRecord]? = []
    @State var profileImage: UIImage? = nil

    @Binding var profile: ProfileRecord

    var body: some View {
        HStack {
            ProfilePicture(name: self.$profile.name ?? "", picture: self.$profileImage, editing: .constant(false) , size: .normal)

            VStack(alignment: .leading) {
                Text(self.profile.name ?? "")
                    .font(.system(.headline))

                Spacer()

                if (self.allergies?.count ?? 0 > 0) {
                    HStack {
                        ForEach(self.allergies ?? [], id: \.self) { (allergy: AllergyRecord) in
                            AllergyPicture(name: State(initialValue: allergy.name ?? ""), fullSize: false)
                        }
                    }
                } else {
                    Text("No allergies")
                        .font(.system(size: 17, weight: .semibold, design: .default))
                        .foregroundColor(.systemGray)
                }
            }
            .padding([.leading, .trailing], 10)

            Spacer()
        }
        .padding([.top, .bottom], 15)
        .onAppear(perform: {
            self.allergies = self.allergyService.fetchProfileAllergies(profile: self.profile)
            self.profileImage = UIImage(data: self.profile.image ?? Data())
        })
    }
}

//struct ProfileRow_Previews: PreviewProvider {
//    static var previews: some View {
//        ProfileRow(profile: .constant(ProfileRecord()))
//    }
//}
