//
//  BarcodeScannerViewController.swift
//  ALLergen
//
//  Created by Charles AUBERT on 1/12/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import UIKit
import SwiftUI
import BarcodeScanner

struct BarcodeScannerView: UIViewControllerRepresentable {
    @Binding var torchMode: TorchMode
    @Binding var product: Product
    @Binding var productCardPosition: CardPosition
    @Binding var maxProductCardPosition: CardPosition
    @Binding var loading: Bool

    func makeUIViewController(context: Context) -> BarcodeScannerViewController {
        let viewController = BarcodeScannerViewController()

        viewController.codeDelegate = context.coordinator
        viewController.errorDelegate = context.coordinator
        viewController.dismissalDelegate = context.coordinator
        viewController.messageViewController.view.isHidden = true
        return viewController
    }

    func updateUIViewController(_ controller: BarcodeScannerViewController, context: Context) {
        controller.cameraViewController.torchMode = self.torchMode
    }

    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }

    class Coordinator: NSObject, BarcodeScannerCodeDelegate, BarcodeScannerErrorDelegate, BarcodeScannerDismissalDelegate {
        var parent: BarcodeScannerView
        var loaded = false
        let productService = ProductService()
        var code: String = ""

        init(_ barcodeScannerView: BarcodeScannerView) {
            self.parent = barcodeScannerView
        }

        func scanner(_ controller: BarcodeScannerViewController, didCaptureCode code: String, type: String) {
            if (self.code == code) {
                self.parent.productCardPosition = .middle
                return
            }
            self.code = code
            self.parent.product.reset()
            self.parent.productCardPosition = .middle
            self.parent.maxProductCardPosition = .middle
            self.parent.loading = true
            self.productService.getProductFromBarcode(barcode: code, preview: true, onSuccess: {(productResponse: ProductResponse) in
                self.parent.loading = false
                if (productResponse.statusMessage ?? "" != "product found") {
                    self.parent.product.valid = false
                } else if let product = productResponse.product {
                    self.parent.product = product
                    self.parent.maxProductCardPosition = self.parent.product.validate() ? .top : .middle
                }
            }, onError: {(error: Error) in
                print(error)
                self.parent.loading = false
                self.parent.maxProductCardPosition = self.parent.product.validate() ? .top : .middle
            })
        }

        func scanner(_ controller: BarcodeScannerViewController, didReceiveError error: Error) {
            print(error)
        }

        func scannerDidDismiss(_ controller: BarcodeScannerViewController) {
            print("Scanner dismissed.")
        }
    }
}
