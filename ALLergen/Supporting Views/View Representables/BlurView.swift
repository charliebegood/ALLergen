//
//  BlurView.swift
//  ALLergen
//
//  Created by Charles AUBERT on 12/4/19.
//  Copyright © 2019 Charles AUBERT. All rights reserved.
//

import UIKit
import SwiftUI

struct BlurView: UIViewRepresentable {
    @Binding var style: UIBlurEffect.Style

    func makeUIView(context: UIViewRepresentableContext<BlurView>) -> UIView {
        let view = UIView(frame: .zero)
        let blurEffect = UIBlurEffect(style: style)
        let blurView = UIVisualEffectView(effect: blurEffect)

        blurView.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .clear
        view.insertSubview(blurView, at: 0)
        NSLayoutConstraint.activate([
            blurView.heightAnchor.constraint(equalTo: view.heightAnchor),
            blurView.widthAnchor.constraint(equalTo: view.widthAnchor),
        ])
        return view
    }

    func updateUIView(_ uiView: UIView,
                      context: UIViewRepresentableContext<BlurView>) {
        for view in uiView.subviews {
            if let blurView = view as? UIVisualEffectView {
                blurView.effect = UIBlurEffect(style: self.style)
            }
        }
    }
}
