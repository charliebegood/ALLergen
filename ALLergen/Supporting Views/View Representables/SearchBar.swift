//
//  SearchBar.swift
//  ALLergen
//
//  Created by Charles AUBERT on 1/14/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import UIKit
import SwiftUI
import Combine

enum SearchBarState {
    case inactive
    case typing
    case canceled
    case deleted
    case validated
}

struct SearchBarStateManager {
    var textInput: String = ""
    var state: SearchBarState = .inactive
    let stateChanged: PassthroughSubject = PassthroughSubject<SearchBarState, Never>()

    mutating func changeState(newState: SearchBarState) {
        self.state = newState
        self.stateChanged.send(newState)
    }
}

struct SearchBar: UIViewRepresentable {
    @Binding var searchBarStateManager: SearchBarStateManager

    func makeUIView(context: Context) -> UISearchBar {
        let searchBar = UISearchBar()

        searchBar.backgroundImage = UIImage()
        searchBar.delegate = context.coordinator
        searchBar.tintColor = UIColor.init(named: "Light Green")
        searchBar.placeholder = "Ingredients, Brands, Categories"
        return searchBar
    }

    func updateUIView(_ searchBar: UISearchBar, context: Context) {
        searchBar.text = self.searchBarStateManager.textInput

        if (searchBar.isFirstResponder && self.searchBarStateManager.state == .validated) {
            searchBar.resignFirstResponder()
            searchBar.setShowsCancelButton(true, animated: true)
        }

        if (!searchBar.isFirstResponder && self.searchBarStateManager.textInput == "") {
            searchBar.setShowsCancelButton(false, animated: true)
        }

        if (!searchBar.isFirstResponder && searchBar.showsCancelButton) {
            if let cancelButton = searchBar.value(forKey: "cancelButton") as? UIButton {
                cancelButton.isEnabled = true
            }
        }
    }

    func makeCoordinator() -> Coordinator {
        Coordinator(self)
    }

    class Coordinator: NSObject, UISearchBarDelegate {
        var parent: SearchBar

        init(_ searchBar: SearchBar) {
            self.parent = searchBar
        }

        func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
            searchBar.setShowsCancelButton(true, animated: true)
            self.parent.searchBarStateManager.changeState(newState: .typing)
        }

        func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
            searchBar.resignFirstResponder()
            self.parent.searchBarStateManager.changeState(newState: .validated)
        }

        func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
            searchBar.setShowsCancelButton(false, animated: true)
            searchBar.resignFirstResponder()
            self.parent.searchBarStateManager.textInput = ""
            self.parent.searchBarStateManager.changeState(newState: .canceled)
        }

        func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
            self.parent.searchBarStateManager.textInput = searchText
            if (searchText == "") {
                self.parent.searchBarStateManager.changeState(newState: .deleted)
            } else {
                self.parent.searchBarStateManager.changeState(newState: .typing)
            }
        }
    }
}
