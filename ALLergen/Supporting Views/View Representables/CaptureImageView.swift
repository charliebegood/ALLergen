//
//  CaptureImageView.swift
//  ALLergen
//
//  Created by Charles AUBERT on 5/10/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import UIKit
import SwiftUI

struct CaptureImageView: UIViewControllerRepresentable {
    @Binding var isShown: Bool
    @Binding var image: UIImage?
    
    func makeCoordinator() -> Coordinator {
        return Coordinator(isShown: $isShown, image: $image)
    }
    
    func makeUIViewController(context: UIViewControllerRepresentableContext<CaptureImageView>) -> UIImagePickerController {
        let picker = UIImagePickerController()
        picker.delegate = context.coordinator
        picker.sourceType = .camera
        picker.allowsEditing = true
        return picker
    }
    
    func updateUIViewController(_ uiViewController: UIImagePickerController, context: UIViewControllerRepresentableContext<CaptureImageView>) {
    }
    
    class Coordinator: NSObject, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
        @Binding var isCoordinatorShown: Bool
        @Binding var imageInCoordinator: UIImage?
        
        init(isShown: Binding<Bool>, image: Binding<UIImage?>) {
            _isCoordinatorShown = isShown
            _imageInCoordinator = image
        }
        
        func imagePickerController(_ picker: UIImagePickerController,
                    didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

            if let img = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
                imageInCoordinator = img
            } else if let img = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
                imageInCoordinator = img
            } else {
                return
            }
            isCoordinatorShown = false
        }
        
        func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
            isCoordinatorShown = false
        }
    }
}
