//
//  StringModifiers.swift
//  ALLergen
//
//  Created by Charles AUBERT on 4/17/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import Foundation

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }

    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
