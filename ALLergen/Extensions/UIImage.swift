//
//  UIImage.swift
//  ALLergen
//
//  Created by Charles AUBERT on 5/10/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import Foundation
import UIKit
import CoreImage

extension UIImage {
    func scaledImage(_ maxDimension: CGFloat) -> UIImage? {
        var scaledSize = CGSize(width: maxDimension, height: maxDimension)
        
        if size.width > size.height {
          scaledSize.height = size.height / size.width * scaledSize.width
        } else {
          scaledSize.width = size.width / size.height * scaledSize.height
        }
        UIGraphicsBeginImageContext(scaledSize)
        draw(in: CGRect(origin: .zero, size: scaledSize))
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return scaledImage
    }
    
    func preprocessedImage() -> UIImage? {
        let inputImage = CIImage(image: self) ?? CIImage(color: .white)
        let blurred =  inputImage.applyingFilter("CIBoxBlur", parameters: [kCIInputRadiusKey: 5]) // block size
        let filter = SmoothThreshold()
        
        filter.inputImage = blurred

        return UIImage(ciImage: filter.outputImage)
    }
}

class SmoothThreshold: CIFilter {
    var inputImage : CIImage?
    var inputEdgeO: CGFloat = 0.25
    var inputEdge1: CGFloat = 0.75

    var colorKernel = CIColorKernel(source:
        "kernel vec4 color(__sample pixel, float inputEdgeO, float inputEdge1)" +
        "{" +
        "    float luma = dot(pixel.rgb, vec3(0.2126, 0.7152, 0.0722));" +
        "    float threshold = smoothstep(inputEdgeO, inputEdge1, luma);" +
        "    return vec4(threshold, threshold, threshold, 1.0);" +
        "}"
    )

    override var outputImage: CIImage! {
        guard let inputImage = inputImage,
            let colorKernel = colorKernel else
        {
            return nil
        }

        let extent = inputImage.extent
        let arguments = [inputImage, inputEdgeO, inputEdge1] as [Any]

        return colorKernel.apply(extent: extent, arguments: arguments)
    }
}
