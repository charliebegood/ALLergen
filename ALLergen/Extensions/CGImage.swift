//
//  CGImage.swift
//  ALLergen
//
//  Created by Charles AUBERT on 5/10/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import Foundation
import UIKit

extension CGImagePropertyOrientation {
    init(_ uiImageOrientation: UIImage.Orientation) {
        switch uiImageOrientation {
        case .up: self = .up
        case .down: self = .down
        case .left: self = .left
        case .right: self = .right
        case .upMirrored: self = .upMirrored
        case .downMirrored: self = .downMirrored
        case .leftMirrored: self = .leftMirrored
        case .rightMirrored: self = .rightMirrored
        @unknown default:
            self = CGImagePropertyOrientation.up
        }
    }
}
