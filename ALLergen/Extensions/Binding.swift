//
//  Binding.swift
//  ALLergen
//
//  Created by Charles AUBERT on 4/23/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import SwiftUI

func ??<T>(lhs: Binding<Optional<T>>, rhs: T) -> Binding<T> {
    Binding(
        get: { lhs.wrappedValue ?? rhs },
        set: { lhs.wrappedValue = $0 }
    )
}
