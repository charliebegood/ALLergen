//
//  KeyboardAdaptiveView.swift
//  ALLergen
//
//  Created by Charles AUBERT on 4/24/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import SwiftUI
import Combine

struct KeyboardAdaptive: ViewModifier {
    @State private var bottomPadding: CGFloat = 0

    func body(content: Content) -> some View {
        content
            .offset(x: 0, y: -self.bottomPadding)
            .onReceive(Publishers.keyboardHeight) { keyboardHeight in
                let keyboardTop = UIScreen.main.bounds.height - keyboardHeight
                let focusedTextInputBottom = UIResponder.currentFirstResponder?.globalFrame?.maxY ?? 0
                self.bottomPadding = max(0, focusedTextInputBottom - keyboardTop + 10)
            }
            .animation(.easeOut(duration: 0.16))
    }
}

extension View {
    func keyboardAdaptive() -> some View {
        ModifiedContent(content: self, modifier: KeyboardAdaptive())
    }
}

extension UIResponder {
    static var currentFirstResponder: UIResponder? {
        _currentFirstResponder = nil
        UIApplication.shared.sendAction(#selector(UIResponder.findFirstResponder(_:)), to: nil, from: nil, for: nil)
        return _currentFirstResponder
    }

    private static weak var _currentFirstResponder: UIResponder?

    @objc private func findFirstResponder(_ sender: Any) {
        UIResponder._currentFirstResponder = self
    }

    var globalFrame: CGRect? {
        guard let view = self as? UIView else { return nil }
        return view.superview?.convert(view.frame, to: nil)
    }
}
