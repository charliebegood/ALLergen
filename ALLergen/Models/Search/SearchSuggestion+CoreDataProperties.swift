//
//  SearchSuggestion+CoreDataProperties.swift
//  
//
//  Created by Charles AUBERT on 1/14/20.
//
//

import Foundation
import CoreData

extension SearchSuggestion {
    @nonobjc public class func createFetchRequest() -> NSFetchRequest<SearchSuggestion> {
        return NSFetchRequest<SearchSuggestion>(entityName: "SearchSuggestion")
    }

    @NSManaged public var title: String?
    @NSManaged public var date: NSDate?
}
