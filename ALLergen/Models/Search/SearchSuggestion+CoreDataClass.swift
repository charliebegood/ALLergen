//
//  SearchSuggestion+CoreDataClass.swift
//  
//
//  Created by Charles AUBERT on 1/14/20.
//
//

import Foundation
import CoreData

@objc(SearchSuggestion)
public class SearchSuggestion: NSManagedObject, Identifiable {
    public var id: UUID = UUID()
}
