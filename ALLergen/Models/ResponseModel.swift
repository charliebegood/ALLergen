//
//  Response.swift
//  ALLergen
//
//  Created by Charles AUBERT on 1/13/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import Foundation
import ObjectMapper
import Combine

class ProductResponse: Mappable {
    var status: Int?
    var product: Product?
    var statusMessage: String?
    var code: String?
    
    required init?(map: Map) {
    }

    func mapping(map: Map) {
        status <- map[ProductResponseFields.status]
        statusMessage <- map[ProductResponseFields.statusMessage]
        code <- map[ProductResponseFields.code]
        product <- map[ProductResponseFields.product]
    }
}

class SearchResponse: Mappable, ObservableObject {
    let newProducts = PassthroughSubject<[Product]?, Never>()
    var page: Int?
    @Published var products: [Product]? {
        didSet {
            self.newProducts.send(self.products)
        }
    }
    var pageSize: Int?
    var count: Int?
    var skip: Int?
    
    required init?(map: Map) {
    }

    func mapping(map: Map) {
        page <- map[SearchResponseFields.page]
        products <- map[SearchResponseFields.products]
        pageSize <- map[SearchResponseFields.pageSize]
        count <- map[SearchResponseFields.count]
        skip <- map[SearchResponseFields.skip]
    }
}

class TranslationResponse: Mappable {
    var code: Int?
    var lang: String?
    var text: [String]?

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        code <- map[TranslationResponseFields.code]
        lang <- map[TranslationResponseFields.lang]
        text <- map[TranslationResponseFields.text]
    }
}
