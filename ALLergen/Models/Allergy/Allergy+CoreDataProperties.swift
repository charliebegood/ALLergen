//
//  Allergy+CoreDataProperties.swift
//  
//
//  Created by Charles AUBERT on 1/16/20.
//
//

import Foundation
import CoreData


extension Allergy {
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Allergy> {
        return NSFetchRequest<Allergy>(entityName: "Allergy")
    }

    @NSManaged public var name: String?
    @NSManaged public var image: String?
}
