//
//  AllergyModel.swift
//  ALLergen
//
//  Created by Charles AUBERT on 1/16/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import Foundation
import ObjectMapper
import CoreData

class Allergy: Identifiable, Hashable {
    var id: UUID
    var name: String
    
    init(name: String) {
        self.id = UUID()
        self.name = name
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.id)
    }
    
    static func == (lhs: Allergy, rhs: Allergy) -> Bool {
        return lhs.name == rhs.name
    }
    
    static func == (lhs: Allergy, rhs: AllergyRecord) -> Bool {
        return lhs.name == (rhs.name ?? "")
    }
}

extension AllergyRecord: Identifiable {
    static func == (lhs: AllergyRecord, rhs: Allergy) -> Bool {
        return (lhs.name ?? "") == rhs.name
    }
    
    static func == (lhs: AllergyRecord, rhs: AllergyRecord) -> Bool {
        return (lhs.name ?? "") == (rhs.name ?? "")
    }
}
