//
//  Product.swift
//  ALLergen
//
//  Created by Charles AUBERT on 1/13/20.
//  Copyright © 2020 Charles AUBERT. All rights reserved.
//

import Foundation
import ObjectMapper
import Combine

class Product: Mappable, Identifiable, Hashable, ObservableObject {
    var id: UUID = UUID()
    var name: String?
    var code: String?
    var brands: String?
    @Published var ingredients: String?
    var frontImage: String?
    @Published var valid: Bool = false

    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        name <- map[ProductFields.name]
        code <- map[ProductFields.code]
        brands <- map[ProductFields.brands]
        ingredients <- map[ProductFields.ingredients]
        frontImage <- map[ProductFields.frontImage]
    }
    
    func validate() -> Bool {
        if (self.name ?? "" == "" || self.brands ?? "" == "" || self.ingredients ?? "" == "") {
            self.valid = false
            return false
        } else {
            self.valid = true
            return true
        }
    }
    
    func reset() {
        self.name = ""
        self.ingredients = ""
        self.brands = ""
        self.frontImage = ""
        self.code = ""
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.id)
    }
    
    static func == (lhs: Product, rhs: Product) -> Bool {
        return lhs.id == rhs.id
    }
}
